# RD53B Workbench
Tools and scripts for performing analysis over RD53B.

Combines [YARR](https://gitlab.cern.ch/YARR/YARR) and [labRemote](https://gitlab.cern.ch/berkeleylab/labRemote/-/tree/master/src)
(via [rd53b_anamon](https://gitlab.cern.ch/berkeleylab/labremote-apps/rd53b_anamon)).

<!----------------------------------------------------------------------------->
<!----------------------------------------------------------------------------->
<!----------------------------------------------------------------------------->
# Table of Contents
 * [Installation](#installation)
   * [Checkout the Code](#check-out-the-code)
   * [Install](#install)
 * [Run the Workbench](#run-the-workbench)

# Installation

## Checkout the Code
```bash
git clone --recursive https://:@gitlab.cern.ch:8443/dantrim/rd53b_workbench.git # change URL as appropriate
```

## Install
```bash
cd path/to/rd53b_workbench/
source /opt/rh/devtoolset-7/enable # gcc 7
mkdir build
cd build
cmake3 ..
make -j4
```

# Run the Workbench
Upon successfull compilation, executables defined under [src/tools/](src/tools/) 
are available via the `workbench` utility which will be located in the `build/`
directory. The utility provides access to the built executables via sub-commands.
All available sub-commands can be printed out by providing the `-l|--list` option
to the `workbench` utility:
```bash
./workbench -l
Available sub-commands:
  clear-tot-mem
  foo-bar
```
Any options and arguments passed to `workbench` _after_ a specific sub-command
will be passed directly to the program associated with the provided sub-command.
For example:
```bash
./workbench clear-tot-mem --help
```
will run print the help message of the program associated with the sub-command
`clear-tot-mem`.