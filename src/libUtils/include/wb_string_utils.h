#ifndef WORKBENCH_STRINGUTILS_H
#define WORKBENCH_STRINGUTILS_H

// std/stl
#include <algorithm>
#include <cctype>
#include <locale>
#include <string>
#include <vector>

namespace rd53b {
namespace utils {

std::string replace_substr(std::string input, std::string old_substring,
                           std::string new_substring);
std::string remove_substr(std::string input_str, std::string sub_str);
std::vector<std::string> split(std::string s, std::string delim);

// trim from start (in place)
inline void ltrim(std::string &s);

// trim from end (in place)
inline void rtrim(std::string &s);

// trim from both ends (in place)
inline void trim(std::string &s);

// trim from start (copying)
inline std::string ltrim_copy(std::string s);

// trim from end (copying)
inline std::string rtrim_copy(std::string s);

// trim from both ends (copying)
inline std::string trim_copy(std::string s);

}  // namespace utils
}  // namespace rd53b

#endif  // WORKBENCH_STRINGUTILS_H
