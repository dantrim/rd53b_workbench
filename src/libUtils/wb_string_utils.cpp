#include "wb_string_utils.h"

namespace rd53b {
namespace utils {

// trim from start (in place)
void ltrim(std::string &s) {
    s.erase(s.begin(), std::find_if(s.begin(), s.end(),
                                    [](int ch) { return !std::isspace(ch); }));
}

// trim from end (in place)
void rtrim(std::string &s) {
    s.erase(std::find_if(s.rbegin(), s.rend(),
                         [](int ch) { return !std::isspace(ch); })
                .base(),
            s.end());
}

// trim from both ends (in place)
void trim(std::string &s) {
    ltrim(s);
    rtrim(s);
}

// trim from start (copying)
std::string ltrim_copy(std::string s) {
    ltrim(s);
    return s;
}

// trim from end (copying)
std::string rtrim_copy(std::string s) {
    rtrim(s);
    return s;
}

// trim from both ends (copying)
std::string trim_copy(std::string s) {
    trim(s);
    return s;
}

std::string replace_substr(std::string in, std::string old_sub,
                           std::string new_sub) {
    std::string out = in;
    while (out.find(old_sub) != std::string::npos) {
        size_t idx = out.find(old_sub);
        out.replace(out.begin() + idx, out.begin() + idx + old_sub.length(),
                    new_sub);
    }
    return out;
}

std::string remove_substr(std::string input_str, std::string sub_str) {
    // from
    // https://stackoverflow.com/questions/32435003/how-to-remove-all-substrings-from-a-string
    std::string out = input_str;
    std::string::size_type n = sub_str.length();
    for (std::string::size_type i = out.find(sub_str); i != std::string::npos;
         i = out.find(sub_str)) {
        out.erase(i, n);
    }
    return out;
}

std::vector<std::string> split(std::string s, std::string delim) {
    std::vector<std::string> out;
    if (s.find(delim) == std::string::npos) {
        out.push_back(rd53b::utils::trim_copy(s));
        return out;
    }

    size_t pos = 0;
    std::string token;
    while ((pos = s.find(delim)) != std::string::npos) {
        token = s.substr(0, pos);
        rd53b::utils::trim(token);
        s.erase(0, pos + delim.length());
        out.push_back(token);
    }
    if (s.length() != 0) out.push_back(s);
    return out;
}

}  // namespace utils
}  // namespace rd53b
