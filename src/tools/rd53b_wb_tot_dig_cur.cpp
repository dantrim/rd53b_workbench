// scc analog monitor card
#include "RD53BAnalogMonitor.h"

// workbench
#include "EquipmentHelpers.h"
#include "Histogram1D.h"
#include "RD53BHelpers.h"
#include "wb_file_utils.h"
#include "wb_string_utils.h"

// labremote
#include "DataSinkConf.h"
#include "IDataSink.h"
#include "Logger.h"
#include "PowerSupplyChannel.h"

// std/stl
#include <getopt.h>
#include <math.h>

#include <algorithm>
#include <experimental/filesystem>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <map>
#include <memory>
#include <sstream>
#include <string>
#include <vector>
namespace fs = std::experimental::filesystem;

// json // handled by YARR
#include <nlohmann/json.hpp>
// using json = nlohmann::json;

// yarr
#include "Rd53b.h"

struct option longopts_t[] = {{"hw", required_argument, NULL, 'c'},
                              {"chip", required_argument, NULL, 'r'},
                              {"debug", no_argument, NULL, 'd'},
                              {"equip", required_argument, NULL, 'e'},
                              {"id", required_argument, NULL, 'i'},
                              {"help", no_argument, NULL, 'h'},
                              {0, 0, 0, 0}};

void print_help() {
    std::cout << "=========================================================="
              << std::endl;
    std::cout << " RD53B power-cycle" << std::endl;
    std::cout << std::endl;
    std::cout << " Usage: [CMD] [OPTIONS]" << std::endl;
    std::cout << std::endl;
    std::cout << " Options:" << std::endl;
    std::cout << "   --hw            JSON configuration file for hw controller"
              << std::endl;
    std::cout << "   --chip          JSON connectivity configuration for RD53B"
              << std::endl;
    std::cout << "   --equip         JSON configuration for lab equipment (PS)"
              << std::endl;
    std::cout << "   -i|--id            Provide chip ID (decimal!)"
              << std::endl;
    std::cout << "   -d|--debug      turn on debug-level" << std::endl;
    std::cout << "   -h|--help       print this help message" << std::endl;
    std::cout << "=========================================================="
              << std::endl;
}

std::map<std::string, unsigned int> dac_map() {
    std::map<std::string, unsigned int> out{
        {"VCAL_HI", 12},           {"VCAL_MED", 12},
        {"DIFF_FE_VTH2", 10},      {"DIFF_FE_VTH1_MAIN", 10},
        {"DIFF_FE_VTH1_LEFT", 10}, {"DIFF_FE_VTH1_RIGHT", 10}};
    return out;
}

enum class VMUX_SEL {
    VREF_ADC_GADC = 0,
    IMUX_PAD_V = 1,
    NTC_PAD_V = 2,
    VREF_ADC_VCAL_DAC = 3,
    VDDA_HALF_CAPMEAS = 4,
    POLY_TEMPSENS_TOP = 5,
    POLY_TEMPSENS_BOT = 6,
    VCAL_HI = 7,
    VCAL_MED = 8,
    DIFF_FE_VTH2 = 9,
    DIFF_FE_VTH1_MAIN = 10,
    DIFF_FE_VTH1_LEFT = 11,
    DIFF_FE_VTH1_RIGHT = 12,
    RADSENSE_ANA_SLDO = 13  // 101
    ,
    TEMPSENSE_ANA_SLDO = 14  // 101
    ,
    RADSENSE_DIG_SLDO = 15  // 101
    ,
    TEMPSENSE_DIG_SLDO = 16  // 101
    ,
    RADSENSE_CENTER = 17  // 102
    ,
    TEMPSENSE_CENTER = 18  // 102
    ,
    ANA_GND_0 = 19,
    ANA_GND_1 = 20,
    ANA_GND_2 = 21,
    ANA_GND_3 = 22,
    ANA_GND_4 = 23,
    ANA_GND_5 = 24,
    ANA_GND_6 = 25,
    ANA_GND_7 = 26,
    ANA_GND_8 = 27,
    ANA_GND_9 = 28,
    ANA_GND_10 = 29,
    ANA_GND_11 = 30,
    VREF_CORE = 31,
    VREF_PRE = 32,
    VINA_HALF = 33,
    VDDA_HALF = 34,
    VREFA = 35,
    VOFS_HALF = 36,
    VIND_HALF = 37,
    VDDD_HALF = 38,
    VREFD = 39,
    VMUX_SEL_INVALID = 99
};

std::map<VMUX_SEL, Rd53bReg Rd53bGlobalCfg::*> vmux_reg_map = {
    {VMUX_SEL::VCAL_HI, &Rd53b::InjVcalHigh},
    {VMUX_SEL::VCAL_MED, &Rd53b::InjVcalMed},
    {VMUX_SEL::DIFF_FE_VTH2, &Rd53b::DiffTh2},
    {VMUX_SEL::DIFF_FE_VTH1_MAIN, &Rd53b::DiffTh1M},
    {VMUX_SEL::DIFF_FE_VTH1_LEFT, &Rd53b::DiffTh1L},
    {VMUX_SEL::DIFF_FE_VTH1_RIGHT, &Rd53b::DiffTh1R}};

enum class IMUX_SEL {
    IREF_MAIN = 0,
    CDR_VCO_MAIN_BIAS = 1,
    CDR_VCO_BUFFER_BIAS = 2,
    CDR_CP_CURRENT = 3,
    CDR_FD_CURRENT = 4,
    CDR_BUFFER_BIAS = 5,
    CML_DRIVER_TAP2_BIAS = 6,
    CML_DRIVER_TAP1_BIAS = 7,
    CML_DRIVER_MAIN_BIAS = 8,
    NTC_PAD_CURRENT = 9,
    CAPMEAS_CIRCUIT = 10,
    CAPMEAS_PARASITIC = 11,
    DIFF_FE_PREAMP_MAIN = 12,
    DIFF_FE_PRECOMP = 13,
    DIFF_FE_COMP = 14,
    DIFF_FE_VTH2 = 15,
    DIFF_FE_VTH1_MAIN = 16,
    DIFF_FE_LCC = 17,
    DIFF_FE_FEEDBACK = 18,
    DIFF_FE_PREAMP_LEFT = 19,
    DIFF_FE_VTH1_LEFT = 20,
    DIFF_FE_PREAMP_RIGHT = 21,
    DIFF_FE_PREAMP_TOPLEFT = 22,
    DIFF_FE_VTH1_RGHT = 23,
    DIFF_FE_PREAMP_TOP = 24,
    DIFF_FE_PREAMP_TOPRIGHT = 25,
    INPUT_CURRENT_ANALOG = 28,
    SHUNT_CURRENT_ANALOG = 29,
    INPUT_CURRENT_DIGITAL = 30,
    SHUNT_CURRENT_DIGITAL = 31,
    IMUX_SEL_INVALID = 99
};

VMUX_SEL vmux_from_string(std::string val) {
    VMUX_SEL out{VMUX_SEL::VMUX_SEL_INVALID};
    if (val == "VCAL_HI")
        return VMUX_SEL::VCAL_HI;
    else if (val == "VCAL_MED")
        return VMUX_SEL::VCAL_MED;
    else if (val == "DIFF_FE_VTH2")
        return VMUX_SEL::DIFF_FE_VTH2;
    else if (val == "DIFF_FE_VTH1_MAIN")
        return VMUX_SEL::DIFF_FE_VTH1_MAIN;
    else if (val == "DIFF_FE_VTH1_LEFT")
        return VMUX_SEL::DIFF_FE_VTH1_LEFT;
    else if (val == "DIFF_FE_VTH1_RIGHT")
        return VMUX_SEL::DIFF_FE_VTH1_RIGHT;
    else
        return VMUX_SEL::VMUX_SEL_INVALID;
}

int main(int argc, char* argv[]) {
    std::string program_name = rd53b::utils::remove_substr(
        fs::path(std::string(argv[0])).filename(), "rd53b_wb_");
    std::string chip_config_filename = "";
    std::string hw_config_filename = "";
    std::string ps_config_filename = "";
    unsigned int chip_id = 0;
    bool debug = false;

    int c;
    while ((c = getopt_long(argc, argv, "c:dr:he:t:q:n:s:b:li:", longopts_t,
                            NULL)) != -1) {
        switch (c) {
            case 'c':
                hw_config_filename = optarg;
                break;
            case 'r':
                chip_config_filename = optarg;
                break;
            case 'i':
                chip_id = std::stoi(optarg);
                break;
            case 'd':
                logIt::incrDebug();
                debug = true;
                break;
            case 'e':
                ps_config_filename = optarg;
                break;
            case 'h':
                print_help();
                return 0;
                break;
            case '?':
            default:
                logger(logERROR) << "Invalid command-line argument provided";
                return 1;
        }  // switch
    }      // while

    fs::path hw_config_path(hw_config_filename);
    fs::path chip_config_path(chip_config_filename);
    fs::path ps_config_path(ps_config_filename);
    if (!fs::exists(hw_config_path)) {
        logger(logERROR) << "Provided HW config file (=\"" << hw_config_filename
                         << "\") does not exist!";
        return 1;
    }
    if (!fs::exists(chip_config_path)) {
        logger(logERROR) << "Provided chip config file (=\""
                         << chip_config_filename << "\") does not exist!";
        return 1;
    }
    if (!fs::exists(ps_config_path)) {
        logger(logERROR) << "Provided equipment config file (=\""
                         << ps_config_filename << "\") does not exist!";
        return 1;
    }

    logger(logDEBUG)
        << "======================================================";
    logger(logDEBUG) << "program         : " << program_name;
    logger(logDEBUG) << "chip id         : 0x" << std::hex << chip_id
                     << std::dec;
    logger(logDEBUG) << "hw config       : " << hw_config_filename;
    logger(logDEBUG) << "chip config     : " << chip_config_filename;
    logger(logDEBUG) << "equp config     : " << ps_config_filename;
    logger(logDEBUG)
        << "======================================================";

    //
    // setup the output data path
    //
    // fs::path p_output_data(rd53b::utils::default_data_dir(program_name));
    std::stringstream data_suffix;
    data_suffix << "chip0x" << std::hex << chip_id << std::dec;
    std::string output_data_path =
        rd53b::utils::default_data_dir(program_name, data_suffix.str());
    logger(logDEBUG) << "output data path: " << output_data_path;

    std::unique_ptr<RD53BAnalogMonitor> anamon =
        std::make_unique<RD53BAnalogMonitor>();
    if (!anamon->init(rd53b::utils::default_anamon_configfile())) {
        logger(logERROR)
            << "Failed to initialize SCC analog monitor card control";
        return 1;
    }
    logger(logINFO) << "AnalogMonitorCard initialized!";

    //
    // setup the power supply
    //
    namespace eh = equipment::helpers;
    // std::string ps_name = "Agilent-goosevulture";
    std::string ps_name = "Rigol-inkfish";
    auto ps = eh::ps_init(ps_config_filename, ps_name);
    if (ps == nullptr) {
        logger(logERROR) << "Failed to load power supply configuration";
        return 1;
    }

    auto ps_vdda = eh::ps_channel_init(ps, ps_name, 1);
    auto ps_vddd = eh::ps_channel_init(ps, ps_name, 2);
    std::vector<std::shared_ptr<PowerSupplyChannel>> ps_channels = {ps_vdda,
                                                                    ps_vddd};

    //
    // setup the frontend
    //
    namespace rh = rd53b::helpers;
    auto hw = rh::spec_init(hw_config_filename);
    auto fe = rh::rd53b_init(hw, chip_config_filename);
    auto cfg = dynamic_cast<FrontEndCfg*>(fe.get());

    nlohmann::json histo_config;
    histo_config["histo_config"] = {
        {"bin_width", 0.001}, {"x_lo", 0}, {"x_hi", 2.5}};

    // auto h_start_up = rd53b::histogram1d::histo_init(histo_config);

    //
    // store output measurements
    //
    std::stringstream measurement_name;
    measurement_name << "data_digital_current_ToTclear";

    std::string ds_config_file = rd53b::utils::datasinks_configfile();
    std::ifstream ifs(ds_config_file);
    nlohmann::json j_ds_config = nlohmann::json::parse(ifs);
    auto j_ds = j_ds_config.at("datasinks");
    if (j_ds.find("CSV") != j_ds.end()) {
        j_ds_config["datasinks"]["CSV"]["directory"] = output_data_path;
    }

    DataSinkConf ds;
    ds.setHardwareConfig(j_ds_config);
    std::shared_ptr<IDataSink> dstream = ds.getDataStream(program_name);

    logger(logINFO) << "Starting measurement loop...";

    // clear tot memories
    // rh::clear_tot_memories(hw, fe);
    // std::this_thread::sleep_for(std::chrono::milliseconds(200));

    hw->setCmdEnable(cfg->getTxChannel());
    hw->setTrigEnable(0x0);

    // set monitor enable
    // fe->writeRegister(&Rd53b::MonitorEnable, 1);

    float fraction_start = 10;
    float fraction_end = 100.0;
    float fraction_step = 10;

    for (float ifrac = fraction_start; ifrac < (fraction_end + fraction_step);
         ifrac += fraction_step) {
        if (ifrac > fraction_end) break;
        dstream->startMeasurement(measurement_name.str(),
                                  std::chrono::system_clock::now());

        auto h = rd53b::histogram1d::histo_init(histo_config);
        for (size_t in = 0; in < 10; in++) {
            eh::ps_turn_on(ps_channels, 1.5, 1.8);
            std::this_thread::sleep_for(std::chrono::milliseconds(1000));
            rh::clear_tot_memories(hw, fe, ifrac);
            while (!hw->isCmdEmpty()) {
            }
            // std::this_thread::sleep_for(std::chrono::milliseconds(500));
            double digital_current = ps_channels.at(1)->measureCurrent();
            logger(logINFO)
                << "PercentEn: " << ifrac << " -> " << digital_current;
            // dstream->setField("PercentEn", ifrac);
            // dstream->setField("I_D", digital_current);
            // dstream->setField("I_D", mean_val);
            // dstream->setField("I_D_err", err);
            h(digital_current);
            eh::ps_turn_off(ps_channels);
            std::this_thread::sleep_for(std::chrono::milliseconds(1000));
        }  // in
        double mean_val = rd53b::histogram1d::mean(h);
        double err = rd53b::histogram1d::stddev(h);
        dstream->setField("Percent", ifrac);
        dstream->setField("I_D", mean_val);
        dstream->setField("err", err);

        //        eh::ps_turn_on(ps_channels, 1.5, 1.8);
        //        std::this_thread::sleep_for(std::chrono::milliseconds(200));
        //
        //        // clear tot memories
        //        rh::clear_tot_memories(hw, fe, ifrac);
        //        while (!hw->isCmdEmpty()) {}
        //        std::this_thread::sleep_for(std::chrono::milliseconds(1000));
        //        for(size_t in = 0; in < 10; in++)
        //        {
        //            double digital_current =
        //            ps_channels.at(1)->measureCurrent(); h(digital_current);
        //        }
        //        double mean_val = rd53b::histogram1d::mean(h);
        //        double err_val = rd53b::histogram1d::stddev(h);
        //        dstream->setField("PercentEn", ifrac);
        //        dstream->setField("DigCurr", mean_val);
        //        //dstream->setField("Err", err_val);
        dstream->recordPoint();
        dstream->endMeasurement();

        // eh::ps_turn_off(ps_channels);
        // std::this_thread::sleep_for(std::chrono::milliseconds(200));
    }  // ifrac

    //
    //        for (auto& dac : selected_dacs) {
    //            double measured_dac_value = -1;
    //            double measured_dac_value_err = -1;
    //
    //            unsigned int dac_n_bits = dac_map().at(dac);
    //            int current_dac_max = pow(2, dac_n_bits);
    //            if (dac_setting >= current_dac_max) {
    //                measured_dac_value = -1;
    //                measured_dac_value_err = -1;
    //                over_10bit = true;
    //            } else {
    //                VMUX_SEL vmux_selection{vmux_from_string(dac)};
    //
    //                // monitor current dac
    //                fe->writeRegister(&Rd53b::MonitorV,
    //                                  static_cast<unsigned
    //                                  int>(vmux_selection));
    //                std::this_thread::sleep_for(std::chrono::milliseconds(5));
    //
    //                // set current dac to current value
    //                fe->writeRegister(vmux_reg_map.at(vmux_selection),
    //                dac_setting);
    //                std::this_thread::sleep_for(std::chrono::milliseconds(5));
    //
    //                auto h = rd53b::histogram1d::histo_init(histo_config_val);
    //                for (size_t in = 0; in < n_samples; in++) {
    //                    double readback_val = anamon->read("VMUX_OUT");
    //                    std::this_thread::sleep_for(std::chrono::milliseconds(5));
    //                    h(readback_val);
    //                }
    //                measured_dac_value = rd53b::histogram1d::mean(h);
    //            }
    //            dac_measurements.push_back(measured_dac_value);
    //        }  // loop over dacs
    //
    //        dstream->setField("DAC", dac_setting);
    //        for (size_t i = 0; i < selected_dacs.size(); i++) {
    //            dstream->setField(selected_dacs.at(i),
    //            dac_measurements.at(i));
    //        }
    //        dstream->recordPoint();
    //        dstream->endMeasurement();
    //    }  // idac
    return 0;
}
