// icc analog monitor card
#include "RD53BAnalogMonitor.h"

// workbench
#include "EquipmentHelpers.h"
#include "Histogram1D.h"
#include "RD53BHelpers.h"
#include "wb_file_utils.h"
#include "wb_string_utils.h"

// labremote
#include "Logger.h"
#include "PowerSupplyChannel.h"

// std/stl
#include <getopt.h>
#include <math.h>

#include <algorithm>
#include <experimental/filesystem>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <memory>
#include <sstream>
#include <string>
#include <vector>
namespace fs = std::experimental::filesystem;

// json // handled by YAR
#include <nlohmann/json.hpp>
// using json = nlohmann::json;

// yarr
#include "Rd53b.h"
#include "ScanHelper.h"
#include "SpecController.h"

struct option longopts_t[] = {{"hw", required_argument, NULL, 'c'},
                              {"chip", required_argument, NULL, 'r'},
                              {"debug", no_argument, NULL, 'd'},
                              {"equip", required_argument, NULL, 'e'},
                              {"temp", required_argument, NULL, 't'},
                              {"id", required_argument, NULL, 'q'},
                              {"n-samples", required_argument, NULL, 'n'},
                              {"help", no_argument, NULL, 'h'},
                              {0, 0, 0, 0}};

void print_help() {
    std::cout << "=========================================================="
              << std::endl;
    std::cout << " RD53B power-cycle" << std::endl;
    std::cout << std::endl;
    std::cout << " Usage: [CMD] [OPTIONS]" << std::endl;
    std::cout << std::endl;
    std::cout << " Options:" << std::endl;
    std::cout << "   --hw            JSON configuration file for hw controller"
              << std::endl;
    std::cout << "   --chip          JSON connectivity configuration for RD53B"
              << std::endl;
    std::cout << "   --equip         JSON configuration for lab equipment (PS)"
              << std::endl;
    std::cout << "   --id            chip ID" << std::endl;
    std::cout << "   -n|--n-samples  number of samples to record" << std::endl;
    std::cout << "   -d|--debug      turn on debug-level" << std::endl;
    std::cout << "   -h|--help       print this help message" << std::endl;
    std::cout << "=========================================================="
              << std::endl;
}

int main(int argc, char* argv[]) {
    std::string program_name = rd53b::utils::remove_substr(
        fs::path(std::string(argv[0])).filename(), "rd53b_wb_");
    std::string chip_config_filename = "";
    std::string hw_config_filename = "";
    std::string ps_config_filename = "";
    std::string operating_temp_str = "";
    unsigned int n_samples = 100;
    unsigned int chip_id = 0;
    bool debug = false;

    int c;
    while ((c = getopt_long(argc, argv, "c:dr:he:t:q:n:", longopts_t, NULL)) !=
           -1) {
        switch (c) {
            case 'c':
                hw_config_filename = optarg;
                break;
            case 'r':
                chip_config_filename = optarg;
                break;
            case 'd':
                logIt::incrDebug();
                debug = true;
                break;
            case 'e':
                ps_config_filename = optarg;
                break;
            case 't':
                operating_temp_str = optarg;
                break;
            case 'q':
                chip_id = std::stoi(optarg);
                break;
            case 'n':
                n_samples = std::stoi(optarg);
                break;
            case 'h':
                print_help();
                return 0;
                break;
            case '?':
            default:
                logger(logERROR) << "Invalid command-line argument provided";
                return 1;
        }  // switch
    }      // while

    // check the inputs
    fs::path hw_config_path(hw_config_filename);
    fs::path chip_config_path(chip_config_filename);
    fs::path ps_config_path(ps_config_filename);
    if (!fs::exists(hw_config_path)) {
        logger(logERROR) << "Provided HW config file (=\"" << hw_config_filename
                         << "\") does not exist!";
        return 1;
    }
    if (!fs::exists(chip_config_path)) {
        logger(logERROR) << "Provided chip config file (=\""
                         << chip_config_filename << "\") does not exist!";
        return 1;
    }
    if (!fs::exists(ps_config_path)) {
        logger(logERROR) << "Provided equipment config file (=\""
                         << ps_config_filename << "\") does not exist!";
        return 1;
    }

    //
    // setup the output data path
    //
    // fs::path p_output_data(rd53b::utils::default_data_dir(program_name));
    std::stringstream data_suffix;
    data_suffix << "chip0x" << std::hex << chip_id << std::dec;
    std::string output_data_path =
        rd53b::utils::default_data_dir(program_name, data_suffix.str());

    logger(logDEBUG)
        << "======================================================";
    logger(logDEBUG) << "program         : " << program_name;
    logger(logDEBUG) << "hw config       : " << hw_config_filename;
    logger(logDEBUG) << "chip config     : " << chip_config_filename;
    logger(logDEBUG) << "equp config     : " << ps_config_filename;
    logger(logDEBUG) << "chip id         : " << chip_id;
    logger(logDEBUG) << "n-samples       : " << n_samples;
    logger(logDEBUG) << "output data path: " << output_data_path;
    logger(logDEBUG)
        << "======================================================";

    std::unique_ptr<RD53BAnalogMonitor> anamon =
        std::make_unique<RD53BAnalogMonitor>();
    if (!anamon->init(rd53b::utils::default_anamon_configfile())) {
        logger(logERROR)
            << "Failed to initialize SCC analog monitor card control";
        return 1;
    }
    logger(logINFO) << "AnalogMonitorCard initialized!";

    //
    // setup the power supply
    //
    namespace eh = equipment::helpers;
    // std::string ps_name = "Agilent-goosevulture";
    std::string ps_name = "Rigol-inkfish";
    auto ps = eh::ps_init(ps_config_filename, ps_name);
    if (ps == nullptr) {
        logger(logERROR) << "Failed to load power supply configuration";
        return 1;
    }

    auto ps_vdda = eh::ps_channel_init(ps, ps_name, 1);
    auto ps_vddd = eh::ps_channel_init(ps, ps_name, 2);
    std::vector<std::shared_ptr<PowerSupplyChannel>> ps_channels = {ps_vdda,
                                                                    ps_vddd};
    // eh::ps_turn_on(ps_channels, 1.5, 1.8);

    //
    // setup the frontend
    //
    namespace rh = rd53b::helpers;
    auto hw = rh::spec_init(hw_config_filename);
    auto fe = rh::rd53b_init(hw, chip_config_filename);
    auto cfg = dynamic_cast<FrontEndCfg*>(fe.get());

    //
    // start power-cycle loop
    //
    unsigned measurement_count = 0;
    unsigned measurement_max = n_samples;

    nlohmann::json histo_config;
    histo_config["histo_config"] = {
        {"bin_width", 0.005}, {"x_lo", 0.0}, {"x_hi", 2.0}};
    auto h_start_up = rd53b::histogram1d::histo_init(histo_config);
    auto h_tot_clear = rd53b::histogram1d::histo_init(histo_config);

    float mean = 0.0;

    //
    // store output measurements
    //
    std::stringstream output_filename;
    output_filename << "data_clear_tot_mem_loop.txt";
    fs::path p_output_file(output_data_path);
    p_output_file /= fs::path(output_filename.str());
    std::fstream datafile(std::string(p_output_file),
                          std::fstream::out | std::fstream::trunc);
    datafile << "#" << std::setw(12) << "ID_STARTUP," << std::setw(12)
             << "ID_TOTCLEAR\n";

    ////foo
    logger(logINFO) << "Starting loop...";
    while (measurement_count < measurement_max) {
        eh::ps_turn_on(ps_channels, 1.5, 1.8);
        std::this_thread::sleep_for(std::chrono::milliseconds(200));

        hw->setCmdEnable(cfg->getTxChannel());
        hw->setTrigEnable(0x0);
        fe->writeRegister(&Rd53b::MonitorEnable, 1);

        double digital_current = ps_channels.at(1)->measureCurrent();
        h_start_up(digital_current);

        rh::clear_tot_memories(hw, fe);
        while (!hw->isCmdEmpty()) {
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(1000));

        double digital_current_clear = ps_channels.at(1)->measureCurrent();
        h_tot_clear(digital_current_clear);

        //
        // turn off the power
        //
        eh::ps_turn_off(ps_channels);
        std::this_thread::sleep_for(std::chrono::milliseconds(200));

        if (measurement_count % 10 == 0 || debug) {
            logger(logINFO)
                << "  [" << measurement_count << "/" << measurement_max << "]"
                << " START-UP: " << digital_current
                << " A, POST-TOT-CLEAR: " << digital_current_clear << " A";
        }

        datafile << " " << std::fixed << std::setprecision(4) << std::setw(11)
                 << digital_current;
        datafile << ",";
        datafile << std::fixed << std::setprecision(4) << std::setw(11)
                 << digital_current_clear;
        datafile << "\n";

        measurement_count++;
    }  // while

    float mean_start_up = rd53b::histogram1d::mean(h_start_up);
    float stddev_start_up = rd53b::histogram1d::stddev(h_start_up);

    float mean_tot_clear = rd53b::histogram1d::mean(h_tot_clear);
    float stddev_tot_clear = rd53b::histogram1d::stddev(h_tot_clear);

    logger(logINFO) << "<I_D>:";
    logger(logINFO) << "    START-UP   = " << std::fixed << std::setprecision(4)
                    << " " << mean_start_up << " +/- " << stddev_start_up
                    << " A";
    logger(logINFO) << "    POST_TOT_CLEAR = " << std::fixed
                    << std::setprecision(4) << " " << mean_tot_clear << " +/- "
                    << stddev_tot_clear << " A";

    // plot
    std::stringstream desc;
    desc << "RD53b (0x" << std::hex << chip_id << std::dec << ") ";
    desc << "tot-mem-clearing";
    nlohmann::json plot_config = {{"labels", {"Digital Current [A]", "[a.u.]"}},
                                  {"color", "#DD4B3C"},
                                  {"alpha", 0.5},
                                  {"grid", true},
                                  {"description", desc.str()},
                                  {"fit", ""}};
    plot_config["plot_config"] = plot_config;

    std::stringstream common_name;
    common_name << "hist_tot_mem_clearing";
    plot_config["plot_config"]["name"] = common_name.str() + "_STARTUP";
    plot_config["plot_config"]["title"] = "Start-up Digital Current";
    h_start_up /= rd53b::histogram1d::n_entries(h_start_up);  // normalize
    rd53b::histogram1d::plot(h_start_up, plot_config, output_data_path);

    plot_config["plot_config"]["name"] = common_name.str() + "_TOTCLEAR";
    plot_config["plot_config"]["title"] = "ToT Cleared Digital Current";
    h_tot_clear /= rd53b::histogram1d::n_entries(h_tot_clear);  // normalize
    rd53b::histogram1d::plot(h_tot_clear, plot_config, output_data_path);

    //
    // turn off the power
    //
    eh::ps_turn_off(ps_channels);

    return 0;
}
