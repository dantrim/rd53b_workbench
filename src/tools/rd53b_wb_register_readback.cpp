// workbench
#include "RD53BHelpers.h"
#include "wb_file_utils.h"

// labremote
#include "Logger.h"

// std/stl
#include <getopt.h>

#include <experimental/filesystem>
#include <memory>  // unique_ptr
#include <string>
#include <vector>
namespace fs = std::experimental::filesystem;

// json // handled by SpecController
//#include <nlohmann/json.hpp>
// using json = nlohmann::json;

#include "Bookkeeper.h"
#include "Rd53b.h"
#include "ScanHelper.h"
#include "SpecController.h"

struct option longopts_t[] = {{"hw", required_argument, NULL, 'c'},
                              {"chip", required_argument, NULL, 'r'},
                              {"debug", no_argument, NULL, 'd'},
                              {"help", no_argument, NULL, 'h'},
                              {0, 0, 0, 0}};

std::pair<uint32_t, uint32_t> MydecodeSingleRegRead(uint32_t higher,
                                                    uint32_t lower);
std::pair<uint32_t, uint32_t> MydecodeSingleRegRead(uint32_t higher,
                                                    uint32_t lower) {
    if ((higher & 0x55000000) == 0x55000000) {
        return std::make_pair((lower >> 16) & 0x3FF, lower & 0xFFFF);
    } else if ((higher & 0x99000000) == 0x99000000) {
        return std::make_pair((higher >> 10) & 0x3FF,
                              ((lower >> 26) & 0x3F) + ((higher & 0x3FF) << 6));
    } else {
        logger(logERROR) << "Could not decode reg read!";
        return std::make_pair(999, 666);
    }
    return std::make_pair(999, 666);
}

void print_help() {
    std::cout << "=========================================================="
              << std::endl;
    std::cout << " RD53B workbench test" << std::endl;
    std::cout << std::endl;
    std::cout << " Usage: [CMD] [OPTIONS]" << std::endl;
    std::cout << std::endl;
    std::cout << " Options:" << std::endl;
    std::cout << "   --hw       JSON configuration file for hw controller"
              << std::endl;
    std::cout << "   --chip     JSON connectivity configuration for RD53B"
              << std::endl;
    std::cout << "   -d|--debug turn on debug-level" << std::endl;
    std::cout << "   -h|--help  print this help message" << std::endl;
    std::cout << "=========================================================="
              << std::endl;
}

int main(int argc, char* argv[]) {
    std::string chip_config_filename = "";
    std::string hw_config_filename = "";
    int c;
    while ((c = getopt_long(argc, argv, "c:dr:h", longopts_t, NULL)) != -1) {
        switch (c) {
            case 'c':
                hw_config_filename = optarg;
                break;
            case 'r':
                chip_config_filename = optarg;
                break;
            case 'd':
                logIt::incrDebug();
                break;
            case 'h':
                print_help();
                return 0;
                break;
            case '?':
            default:
                logger(logERROR) << "Invalid command-line argument provided";
                return 1;
        }  // switch
    }      // while

    // check the inputs
    fs::path hw_config_path(hw_config_filename);
    fs::path chip_config_path(chip_config_filename);
    if (!fs::exists(hw_config_path)) {
        logger(logERROR) << "Provided HW config file (=\"" << hw_config_filename
                         << "\") does not exist!";
        return 1;
    }
    if (!fs::exists(chip_config_path)) {
        logger(logERROR) << "Provided chip config file (=\""
                         << chip_config_filename << "\") does not exist!";
        return 1;
    }

    //
    // setup the frontend
    //

    namespace rh = rd53b::helpers;
    auto hw = rh::spec_init(hw_config_filename);
    auto fe = rh::rd53b_init(hw, chip_config_filename);

    //
    // run the ToT memory clearing routine
    //
    rh::clear_tot_memories(hw, fe);
    std::this_thread::sleep_for(std::chrono::milliseconds(200));
    if (fe->ServiceBlockEn.read() == 0) {
        logger(logERROR)
            << "Register messages not enabled (set ServiceBlockEn == 1)";
        rh::rd53b_reset(hw, fe);
        return 1;
    }

    hw->setupMode();
    auto fe_cfg = dynamic_cast<FrontEndCfg*>(fe.get());
    hw->setTrigEnable(0x0);
    hw->flushBuffer();  // this call is needed for doing the readback
    hw->setCmdEnable(fe_cfg->getTxChannel());
    hw->setRxEnable(fe_cfg->getRxChannel());
    // fe->configure();
    // std::this_thread::sleep_for(std::chrono::microseconds(100));
    // while(!hw->isCmdEmpty()) {}

    auto reg = fe->getNamedRegister("DiffPreampL");
    uint32_t register_address = 8;
    fe->writeRegister(&Rd53b::DiffPreampL, 900);
    // uint32_t register_address = (fe->*reg).addr();
    // uint32_t register_value_initial = (fe->*reg).read();
    // logger(logINFO) << "Initial register value: " << register_value_initial;
    fe->sendRdReg(15, register_address);
    while (!hw->isCmdEmpty()) {
    }
    std::this_thread::sleep_for(std::chrono::milliseconds(10));
    RawData* data = hw->readData();
    if (data != NULL) {
        std::cout << "Data size: " << data->words << std::endl;
        if (!data->words >= 2) {
            // if (!(data->words == 2 || data->words == 4 || data->words == 8 ||
            // data->words == 12 || data->words == 6)) {
            logger(logERROR) << "Received wrong number of words ("
                             << data->words << ") for FE";
            rh::rd53b_reset(hw, fe);
            return 1;
        }
        std::pair<uint32_t, uint32_t> answer =
            MydecodeSingleRegRead(data->buf[0], data->buf[1]);
        logger(logINFO) << "Received data for Addr " << answer.first << " : "
                        << answer.second;
        if (answer.first != register_address) {
            logger(logERROR) << "Received data was not as expected:";
            logger(logERROR) << "    Received Address: " << answer.first
                             << ", expected: " << register_address;
        }
    } else {
        logger(logINFO) << "Data is null!";
    }

    //
    // reset the RD53b back to it's power-on state
    //
    rh::rd53b_reset(hw, fe);

    return 0;
}
