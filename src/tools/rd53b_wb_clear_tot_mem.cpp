// scc analog monitor card
#include "RD53BAnalogMonitor.h"
#include "wb_file_utils.h"

// workbench
#include "RD53BHelpers.h"

// labremote
#include "Logger.h"

// std/stl
#include <getopt.h>

#include <experimental/filesystem>
#include <memory>  // unique_ptr
#include <string>
#include <vector>
namespace fs = std::experimental::filesystem;

// json // handled by SpecController
//#include <nlohmann/json.hpp>
// using json = nlohmann::json;

#include "Bookkeeper.h"
#include "Rd53b.h"
#include "ScanHelper.h"
#include "SpecController.h"

struct option longopts_t[] = {{"hw", required_argument, NULL, 'c'},
                              {"chip", required_argument, NULL, 'r'},
                              {"debug", no_argument, NULL, 'd'},
                              {"help", no_argument, NULL, 'h'},
                              {0, 0, 0, 0}};

void print_help() {
    std::cout << "=========================================================="
              << std::endl;
    std::cout << " RD53B workbench test" << std::endl;
    std::cout << std::endl;
    std::cout << " Usage: [CMD] [OPTIONS]" << std::endl;
    std::cout << std::endl;
    std::cout << " Options:" << std::endl;
    std::cout << "   --hw       JSON configuration file for hw controller"
              << std::endl;
    std::cout << "   --chip     JSON connectivity configuration for RD53B"
              << std::endl;
    std::cout << "   -d|--debug turn on debug-level" << std::endl;
    std::cout << "   -h|--help  print this help message" << std::endl;
    std::cout << "=========================================================="
              << std::endl;
}

int main(int argc, char* argv[]) {
    std::string chip_config_filename = "";
    std::string hw_config_filename = "";
    int c;
    while ((c = getopt_long(argc, argv, "c:dr:h", longopts_t, NULL)) != -1) {
        switch (c) {
            case 'c':
                hw_config_filename = optarg;
                break;
            case 'r':
                chip_config_filename = optarg;
                break;
            case 'd':
                logIt::incrDebug();
                break;
            case 'h':
                print_help();
                return 0;
                break;
            case '?':
            default:
                logger(logERROR) << "Invalid command-line argument provided";
                return 1;
        }  // switch
    }      // while

    // check the inputs
    fs::path hw_config_path(hw_config_filename);
    fs::path chip_config_path(chip_config_filename);
    if (!fs::exists(hw_config_path)) {
        logger(logERROR) << "Provided HW config file (=\"" << hw_config_filename
                         << "\") does not exist!";
        return 1;
    }
    if (!fs::exists(chip_config_path)) {
        logger(logERROR) << "Provided chip config file (=\""
                         << chip_config_filename << "\") does not exist!";
        return 1;
    }

    // std::unique_ptr<RD53BAnalogMonitor> anamon =
    //    std::make_unique<RD53BAnalogMonitor>();
    // if (!anamon->init(rd53b::utils::default_anamon_configfile())) {
    //    logger(logERROR)
    //        << "Failed to initialize SCC analog monitor card control";
    //    return 1;
    //}
    // logger(logINFO) << "AnalogMonitorCard initialized!";

    //
    // setup the frontend
    //

    namespace rh = rd53b::helpers;
    auto hw = rh::spec_init(hw_config_filename);
    auto fe = rh::rd53b_init(hw, chip_config_filename);

    //
    // run the ToT memory clearing routine
    //
    rh::clear_tot_memories(hw, fe);

    //
    // reset the RD53b back to it's power-on state
    //
    rh::rd53b_reset(hw, fe);

    return 0;
}
