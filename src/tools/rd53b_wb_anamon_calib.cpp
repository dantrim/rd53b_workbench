// scc analog monitor card
#include "RD53BAnalogMonitor.h"

// workbench
#include "EquipmentHelpers.h"
#include "Histogram1D.h"
#include "RD53BHelpers.h"
#include "wb_file_utils.h"
#include "wb_string_utils.h"

// labremote
#include "Logger.h"
#include "PowerSupplyChannel.h"

// std/stl
#include <getopt.h>
#include <math.h>

#include <algorithm>
#include <experimental/filesystem>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <memory>
#include <sstream>
#include <string>
#include <vector>
namespace fs = std::experimental::filesystem;

// json // handled by YAR
#include <nlohmann/json.hpp>
// using json = nlohmann::json;
//
struct option longopts_t[] = {{"equip", required_argument, NULL, 'e'},
                              {"debug", no_argument, NULL, 'd'},
                              {"n-samples", required_argument, NULL, 'n'},
                              {"step", required_argument, NULL, 's'},
                              {"help", no_argument, NULL, 'h'},
                              {0, 0, 0, 0}};

void print_help() {
    std::cout << "=========================================================="
              << std::endl;
    std::cout << " RD53B SCC Analog Monitor ADC Calibration Loop" << std::endl;
    std::cout << std::endl;
    std::cout << " Usage: [CMD] [OPTIONS]" << std::endl;
    std::cout << std::endl;
    std::cout << " Options:" << std::endl;
    std::cout << "   --equip         JSON configuration for lab equipment (PS)"
              << std::endl;
    std::cout << "   -s|--step       step size for incrementing set PS voltage"
              << std::endl;
    std::cout << "   -n|--n-samples  number of samples to record" << std::endl;
    std::cout << "   -d|--debug      turn on debug-level" << std::endl;
    std::cout << "   -h|--help       print this help message" << std::endl;
    std::cout << "=========================================================="
              << std::endl;
}

int main(int argc, char* argv[]) {
    std::string program_name = rd53b::utils::remove_substr(
        fs::path(std::string(argv[0])).filename(), "rd53b_wb_");
    std::string ps_config_filename = "";
    unsigned int n_samples = 100;
    float step_size = 0.05;
    bool debug = false;

    int c;
    while ((c = getopt_long(argc, argv, "dhe:n:", longopts_t, NULL)) != -1) {
        switch (c) {
            case 'd':
                logIt::incrDebug();
                debug = true;
                break;
            case 'e':
                ps_config_filename = optarg;
                break;
            case 'n':
                n_samples = std::stoi(optarg);
                break;
            case 'h':
                print_help();
                return 0;
                break;
            case 's':
                step_size = std::stof(optarg);
                break;
            case '?':
            default:
                logger(logERROR) << "Invalid command-line argument provided";
                return 1;
        }  // switch
    }      // while

    // check the inputs
    fs::path ps_config_path(ps_config_filename);
    if (!fs::exists(ps_config_path)) {
        logger(logERROR) << "Provided equipment config file (=\""
                         << ps_config_filename << "\") does not exist!";
        return 1;
    }

    logger(logDEBUG)
        << "======================================================";
    logger(logDEBUG) << "program         : " << program_name;
    logger(logDEBUG) << "equp config     : " << ps_config_filename;
    logger(logDEBUG) << "n-samples       : " << n_samples;
    logger(logDEBUG)
        << "======================================================";

    //
    // setup the output data path
    //
    std::string output_data_path = rd53b::utils::default_data_dir(program_name);
    logger(logDEBUG) << "output data path: " << output_data_path;

    std::unique_ptr<RD53BAnalogMonitor> anamon =
        std::make_unique<RD53BAnalogMonitor>();
    if (!anamon->init(rd53b::utils::default_anamon_configfile())) {
        logger(logERROR)
            << "Failed to initialize SCC analog monitor card control";
        return 1;
    }
    logger(logINFO) << "AnalogMonitorCard initialized!";

    //
    // setup the power supply
    //
    namespace eh = equipment::helpers;
    // std::string ps_name = "Agilent-goosevulture";
    std::string ps_name = "Rigol-inkfish";
    auto ps_base = eh::ps_init(ps_config_filename, ps_name);
    if (ps_base == nullptr) {
        logger(logERROR) << "Failed to load power supply configuration";
        return 1;
    }

    auto ps_vdda = eh::ps_channel_init(ps_base, ps_name, 1);
    auto ps_vddd = eh::ps_channel_init(ps_base, ps_name, 2);

    //
    // start power-cycle loop
    //
    unsigned measurement_count = 0;
    unsigned measurement_max = n_samples;

    nlohmann::json histo_config;
    histo_config["histo_config"] = {
        {"bin_width", 1}, {"x_lo", 0}, {"x_hi", 4096}};
    nlohmann::json histo_config_val;
    histo_config_val["histo_config"] = {
        {"bin_width", 0.001}, {"x_lo", 0.0}, {"x_hi", 3.0}};

    //
    // store output measurements
    //
    std::stringstream output_filename;
    output_filename << "data_anamon_adc_calib.txt";
    fs::path p_output_file(output_data_path);
    p_output_file /= fs::path(output_filename.str());
    std::fstream datafile(std::string(p_output_file),
                          std::fstream::out | std::fstream::trunc);
    datafile << "#" << std::setw(12) << "V_SET," << std::setw(12) << "ADC_CNT,"
             << std::setw(12) << "ADC_CNT_ERR\n";

    ////foo
    logger(logINFO) << "Starting measurement loop...";

    float v_min = 0.0;
    float v_max = 3.0;
    std::vector<float> v_step_vals;
    for (float v = v_min; v <= v_max; v += step_size) {
        if (v > v_max) break;
        v_step_vals.push_back(v);
    }

    for (size_t iv = 0; iv < v_step_vals.size(); iv++) {
        // turn on the set voltage value
        float v_set = v_step_vals.at(iv);
        eh::ps_turn_on({ps_vdda}, v_set, 1.8);
        std::this_thread::sleep_for(std::chrono::milliseconds(100));

        auto h = rd53b::histogram1d::histo_init(histo_config);
        auto h_val = rd53b::histogram1d::histo_init(histo_config_val);
        for (size_t in = 0; in < n_samples; in++) {
            uint16_t vin_counts = anamon->readCount("VDDA");
            double vin_val = anamon->read("VDDA");
            h(vin_counts);
            h_val(vin_val);
        }
        double mean_counts = rd53b::histogram1d::mean(h);
        double stddev_counts = rd53b::histogram1d::stddev(h);
        double mean_val = rd53b::histogram1d::mean(h_val);
        double stddev_val = rd53b::histogram1d::stddev(h_val);
        datafile << " " << std::fixed << std::setprecision(2) << std::setw(11)
                 << v_set;
        datafile << ",";
        datafile << std::fixed << std::setprecision(4) << std::setw(11)
                 << mean_counts;
        datafile << ",";
        datafile << std::fixed << std::setprecision(4) << std::setw(11)
                 << stddev_counts;
        datafile << "\n";

        if (iv % 10 == 0 || debug) {
            logger(logINFO)
                << " [" << iv << "/" << v_step_vals.size() - 1 << "]"
                << " V_SET: " << v_set << " V, COUNTS: " << std::fixed
                << std::setprecision(2) << mean_counts
                << " counts, VAL: " << std::setprecision(2) << mean_val << " V";
        }
    }
    eh::ps_turn_off({ps_vdda});  //, ps_vddd } );

    return 0;
}
