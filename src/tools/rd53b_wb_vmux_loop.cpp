// scc analog monitor card
#include "RD53BAnalogMonitor.h"

// workbench
#include "EquipmentHelpers.h"
#include "Histogram1D.h"
#include "RD53BHelpers.h"
#include "wb_file_utils.h"
#include "wb_string_utils.h"

// labremote
#include "Logger.h"

// std/stl
#include <getopt.h>
#include <math.h>

#include <algorithm>
#include <experimental/filesystem>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <map>
#include <memory>
#include <sstream>
#include <string>
#include <vector>
namespace fs = std::experimental::filesystem;

// json // handled by YARR
#include <nlohmann/json.hpp>

// yarr
#include "Rd53b.h"

struct option longopts_t[] = {{"hw", required_argument, NULL, 'r'},
                              {"chip", required_argument, NULL, 'c'},
                              {"ana-mon", required_argument, NULL, 'a'},
                              {"debug", no_argument, NULL, 'd'},
                              {"n-samples", required_argument, NULL, 'n'},
                              {"help", no_argument, NULL, 'h'},
                              {0, 0, 0, 0}};

void print_help() {
    std::cout << "=========================================================="
              << std::endl;
    std::cout << " RD53B vmux-loop" << std::endl;
    std::cout << std::endl;
    std::cout << " Usage: [CMD] [OPTIONS]" << std::endl;
    std::cout << std::endl;
    std::cout << " Options:" << std::endl;
    std::cout << "   -a|--ana-mon    RD53b analog monitor card JSON "
                 "configuration file [if not provided, a default one is used]"
              << std::endl;
    std::cout << "   -r|--hw            JSON YARR configuration file for hw "
                 "controller [REQUIRED]"
              << std::endl;
    std::cout << "   -c|--chip          JSON YARR connectivity configuration "
                 "for RD53B [REQUIRED]"
              << std::endl;
    std::cout << "   -n|--n-samples  number of samples to record for each "
                 "measurement (reports the average) [default: 5]"
              << std::endl;
    std::cout << "   -d|--debug      turn on debug-level" << std::endl;
    std::cout << "   -h|--help       print this help message" << std::endl;
    std::cout << "=========================================================="
              << std::endl;
}

std::map<std::string, unsigned int> dac_map() {
    std::map<std::string, unsigned int> out{
        {"VCAL_HI", 12},           {"VCAL_MED", 12},
        {"DIFF_FE_VTH2", 10},      {"DIFF_FE_VTH1_MAIN", 10},
        {"DIFF_FE_VTH1_LEFT", 10}, {"DIFF_FE_VTH1_RIGHT", 10}};
    return out;
}

enum class VMUX_SEL {
    VREF_ADC_GADC = 0,
    IMUX_PAD_V = 1,
    NTC_PAD_V = 2,
    VREF_ADC_VCAL_DAC = 3,
    VDDA_HALF_CAPMEAS = 4,
    POLY_TEMPSENS_TOP = 5,
    POLY_TEMPSENS_BOT = 6,
    VCAL_HI = 7,
    VCAL_MED = 8,
    DIFF_FE_VTH2 = 9,
    DIFF_FE_VTH1_MAIN = 10,
    DIFF_FE_VTH1_LEFT = 11,
    DIFF_FE_VTH1_RIGHT = 12,
    RADSENSE_ANA_SLDO = 13  // 101
    ,
    TEMPSENSE_ANA_SLDO = 14  // 101
    ,
    RADSENSE_DIG_SLDO = 15  // 101
    ,
    TEMPSENSE_DIG_SLDO = 16  // 101
    ,
    RADSENSE_CENTER = 17  // 102
    ,
    TEMPSENSE_CENTER = 18  // 102
    ,
    ANA_GND_0 = 19,
    ANA_GND_1 = 20,
    ANA_GND_2 = 21,
    ANA_GND_3 = 22,
    ANA_GND_4 = 23,
    ANA_GND_5 = 24,
    ANA_GND_6 = 25,
    ANA_GND_7 = 26,
    ANA_GND_8 = 27,
    ANA_GND_9 = 28,
    ANA_GND_10 = 29,
    ANA_GND_11 = 30,
    VREF_CORE = 31,
    VREF_PRE = 32,
    VINA_HALF = 33,
    VDDA_HALF = 34,
    VREFA = 35,
    VOFS_HALF = 36,
    VIND_HALF = 37,
    VDDD_HALF = 38,
    VREFD = 39,
    VMUX_SEL_INVALID = 99
};

std::map<VMUX_SEL, Rd53bReg Rd53bGlobalCfg::*> vmux_reg_map = {
    {VMUX_SEL::VCAL_HI, &Rd53b::InjVcalHigh},
    {VMUX_SEL::VCAL_MED, &Rd53b::InjVcalMed},
    {VMUX_SEL::DIFF_FE_VTH2, &Rd53b::DiffTh2},
    {VMUX_SEL::DIFF_FE_VTH1_MAIN, &Rd53b::DiffTh1M},
    {VMUX_SEL::DIFF_FE_VTH1_LEFT, &Rd53b::DiffTh1L},
    {VMUX_SEL::DIFF_FE_VTH1_RIGHT, &Rd53b::DiffTh1R}};

enum class IMUX_SEL {
    IREF_MAIN = 0,
    CDR_VCO_MAIN_BIAS = 1,
    CDR_VCO_BUFFER_BIAS = 2,
    CDR_CP_CURRENT = 3,
    CDR_FD_CURRENT = 4,
    CDR_BUFFER_BIAS = 5,
    CML_DRIVER_TAP2_BIAS = 6,
    CML_DRIVER_TAP1_BIAS = 7,
    CML_DRIVER_MAIN_BIAS = 8,
    NTC_PAD_CURRENT = 9,
    CAPMEAS_CIRCUIT = 10,
    CAPMEAS_PARASITIC = 11,
    DIFF_FE_PREAMP_MAIN = 12,
    DIFF_FE_PRECOMP = 13,
    DIFF_FE_COMP = 14,
    DIFF_FE_VTH2 = 15,
    DIFF_FE_VTH1_MAIN = 16,
    DIFF_FE_LCC = 17,
    DIFF_FE_FEEDBACK = 18,
    DIFF_FE_PREAMP_LEFT = 19,
    DIFF_FE_VTH1_LEFT = 20,
    DIFF_FE_PREAMP_RIGHT = 21,
    DIFF_FE_PREAMP_TOPLEFT = 22,
    DIFF_FE_VTH1_RGHT = 23,
    DIFF_FE_PREAMP_TOP = 24,
    DIFF_FE_PREAMP_TOPRIGHT = 25,
    INPUT_CURRENT_ANALOG = 28,
    SHUNT_CURRENT_ANALOG = 29,
    INPUT_CURRENT_DIGITAL = 30,
    SHUNT_CURRENT_DIGITAL = 31,
    IMUX_SEL_INVALID = 99
};

std::string vmux_to_string(VMUX_SEL val) {
    if (val == VMUX_SEL::VREF_ADC_GADC)
        return "VREF_ADC_GADC";
    else if (val == VMUX_SEL::IMUX_PAD_V)
        return "IMUX_PAD_V";
    else if (val == VMUX_SEL::NTC_PAD_V)
        return "NTC_PAD_V";
    else if (val == VMUX_SEL::VREF_ADC_VCAL_DAC)
        return "VREF_ADC_VCAL_DAC";
    else if (val == VMUX_SEL::VDDA_HALF_CAPMEAS)
        return "VDDA_HALF_CAPMEAS";
    else if (val == VMUX_SEL::POLY_TEMPSENS_TOP)
        return "POLY_TEMPSENS_TOP";
    else if (val == VMUX_SEL::POLY_TEMPSENS_BOT)
        return "POLY_TEMPSENS_BOT";
    else if (val == VMUX_SEL::VCAL_HI)
        return "VCAL_HI";
    else if (val == VMUX_SEL::VCAL_MED)
        return "VCAL_MED";
    else if (val == VMUX_SEL::DIFF_FE_VTH2)
        return "DIFF_FE_VTH2";
    else if (val == VMUX_SEL::DIFF_FE_VTH1_MAIN)
        return "DIFF_FE_VTH1_MAIN";
    else if (val == VMUX_SEL::DIFF_FE_VTH1_LEFT)
        return "DIFF_FE_VTH1_LEFT";
    else if (val == VMUX_SEL::DIFF_FE_VTH1_RIGHT)
        return "DIFF_FE_VTH1_RIGHT";
    else if (val == VMUX_SEL::RADSENSE_ANA_SLDO)
        return "RADSENSE_ANA_SLDO";
    else if (val == VMUX_SEL::TEMPSENSE_ANA_SLDO)
        return "TEMPSENSE_ANA_SLDO";
    else if (val == VMUX_SEL::RADSENSE_DIG_SLDO)
        return "RADSENSE_DIG_SLDO";
    else if (val == VMUX_SEL::TEMPSENSE_DIG_SLDO)
        return "TEMPSENSE_DIG_SLDO";
    else if (val == VMUX_SEL::RADSENSE_CENTER)
        return "RADSENSE_CENTER";
    else if (val == VMUX_SEL::TEMPSENSE_CENTER)
        return "TEMPSENSE_CENTER";
    else if (val == VMUX_SEL::ANA_GND_0)
        return "ANA_GND_0";
    else if (val == VMUX_SEL::ANA_GND_1)
        return "ANA_GND_1";
    else if (val == VMUX_SEL::ANA_GND_2)
        return "ANA_GND_2";
    else if (val == VMUX_SEL::ANA_GND_3)
        return "ANA_GND_3";
    else if (val == VMUX_SEL::ANA_GND_4)
        return "ANA_GND_4";
    else if (val == VMUX_SEL::ANA_GND_5)
        return "ANA_GND_5";
    else if (val == VMUX_SEL::ANA_GND_6)
        return "ANA_GND_6";
    else if (val == VMUX_SEL::ANA_GND_7)
        return "ANA_GND_7";
    else if (val == VMUX_SEL::ANA_GND_8)
        return "ANA_GND_8";
    else if (val == VMUX_SEL::ANA_GND_9)
        return "ANA_GND_9";
    else if (val == VMUX_SEL::ANA_GND_10)
        return "ANA_GND_10";
    else if (val == VMUX_SEL::ANA_GND_11)
        return "ANA_GND_11";
    else if (val == VMUX_SEL::VREF_CORE)
        return "VREF_CORE";
    else if (val == VMUX_SEL::VREF_PRE)
        return "VREF_PRE";
    else if (val == VMUX_SEL::VINA_HALF)
        return "VINA_HALF";
    else if (val == VMUX_SEL::VDDA_HALF)
        return "VDDA_HALF";
    else if (val == VMUX_SEL::VREFA)
        return "VREFA";
    else if (val == VMUX_SEL::VOFS_HALF)
        return "VOFS_HALF";
    else if (val == VMUX_SEL::VIND_HALF)
        return "VIND_HALF";
    else if (val == VMUX_SEL::VDDD_HALF)
        return "VDDD_HALF";
    else if (val == VMUX_SEL::VREFD)
        return "VREFD";
    else
        return "";
}

VMUX_SEL vmux_from_string(std::string val) {
    VMUX_SEL out{VMUX_SEL::VMUX_SEL_INVALID};
    if (val == "VREF_ADC_GADC")
        return VMUX_SEL::VREF_ADC_GADC;
    else if (val == "IMUX_PAD_V")
        return VMUX_SEL::IMUX_PAD_V;
    else if (val == "NTC_PAD_V")
        return VMUX_SEL::NTC_PAD_V;
    else if (val == "VREF_ADC_VCAL_DAC")
        return VMUX_SEL::VREF_ADC_VCAL_DAC;
    else if (val == "VDDA_HALF_CAPMEAS")
        return VMUX_SEL::VDDA_HALF_CAPMEAS;
    else if (val == "POLY_TEMPSENS_TOP")
        return VMUX_SEL::POLY_TEMPSENS_TOP;
    else if (val == "POLY_TEMPSENS_BOT")
        return VMUX_SEL::POLY_TEMPSENS_BOT;
    else if (val == "VCAL_HI")
        return VMUX_SEL::VCAL_HI;
    else if (val == "VCAL_MED")
        return VMUX_SEL::VCAL_MED;
    else if (val == "DIFF_FE_VTH2")
        return VMUX_SEL::DIFF_FE_VTH2;
    else if (val == "DIFF_FE_VTH1_MAIN")
        return VMUX_SEL::DIFF_FE_VTH1_MAIN;
    else if (val == "DIFF_FE_VTH1_LEFT")
        return VMUX_SEL::DIFF_FE_VTH1_LEFT;
    else if (val == "DIFF_FE_VTH1_RIGHT")
        return VMUX_SEL::DIFF_FE_VTH1_RIGHT;
    else if (val == "RADSENSE_ANA_SLDO")
        return VMUX_SEL::RADSENSE_ANA_SLDO;
    else if (val == "TEMPSENSE_ANA_SLDO")
        return VMUX_SEL::TEMPSENSE_ANA_SLDO;
    else if (val == "RADSENSE_DIG_SLDO")
        return VMUX_SEL::RADSENSE_DIG_SLDO;
    else if (val == "TEMPSENSE_DIG_SLDO")
        return VMUX_SEL::TEMPSENSE_DIG_SLDO;
    else if (val == "RADSENSE_CENTER")
        return VMUX_SEL::RADSENSE_CENTER;
    else if (val == "TEMPSENSE_CENTER")
        return VMUX_SEL::TEMPSENSE_CENTER;
    else if (val == "ANA_GND_0")
        return VMUX_SEL::ANA_GND_0;
    else if (val == "ANA_GND_1")
        return VMUX_SEL::ANA_GND_1;
    else if (val == "ANA_GND_2")
        return VMUX_SEL::ANA_GND_2;
    else if (val == "ANA_GND_3")
        return VMUX_SEL::ANA_GND_3;
    else if (val == "ANA_GND_4")
        return VMUX_SEL::ANA_GND_4;
    else if (val == "ANA_GND_5")
        return VMUX_SEL::ANA_GND_5;
    else if (val == "ANA_GND_6")
        return VMUX_SEL::ANA_GND_6;
    else if (val == "ANA_GND_7")
        return VMUX_SEL::ANA_GND_7;
    else if (val == "ANA_GND_8")
        return VMUX_SEL::ANA_GND_8;
    else if (val == "ANA_GND_9")
        return VMUX_SEL::ANA_GND_9;
    else if (val == "ANA_GND_10")
        return VMUX_SEL::ANA_GND_10;
    else if (val == "ANA_GND_11")
        return VMUX_SEL::ANA_GND_11;
    else if (val == "VREF_CORE")
        return VMUX_SEL::VREF_CORE;
    else if (val == "VREF_PRE")
        return VMUX_SEL::VREF_PRE;
    else if (val == "VINA_HALF")
        return VMUX_SEL::VINA_HALF;
    else if (val == "VDDA_HALF")
        return VMUX_SEL::VDDA_HALF;
    else if (val == "VREFA")
        return VMUX_SEL::VREFA;
    else if (val == "VOFS_HALF")
        return VMUX_SEL::VOFS_HALF;
    else if (val == "VIND_HALF")
        return VMUX_SEL::VIND_HALF;
    else if (val == "VDDD_HALF")
        return VMUX_SEL::VDDD_HALF;
    else if (val == "VREFD")
        return VMUX_SEL::VREFD;
    else
        return VMUX_SEL::VMUX_SEL_INVALID;
}

int main(int argc, char* argv[]) {
    std::string program_name = rd53b::utils::remove_substr(
        fs::path(std::string(argv[0])).filename(), "rd53b_wb_");
    std::string chip_config_filename = "";
    std::string hw_config_filename = "";
    std::string anamon_config_filename = "";
    unsigned n_samples = 5;
    bool debug = false;

    int c;
    while ((c = getopt_long(argc, argv, "a:c:dr:hn:", longopts_t, NULL)) !=
           -1) {
        switch (c) {
            case 'a':
                anamon_config_filename = optarg;
                break;
            case 'r':
                hw_config_filename = optarg;
                break;
            case 'c':
                chip_config_filename = optarg;
                break;
            case 'd':
                logIt::incrDebug();
                debug = true;
                break;
            case 'n':
                n_samples = atoi(optarg);
                break;
            case 'h':
                print_help();
                return 0;
                break;
            case '?':
            default:
                logger(logERROR) << "Invalid command-line argument provided";
                return 1;
        }  // switch
    }      // while

    fs::path hw_config_path(hw_config_filename);
    fs::path chip_config_path(chip_config_filename);
    if (!fs::exists(hw_config_path)) {
        logger(logERROR) << "Provided HW config file (=\"" << hw_config_filename
                         << "\") does not exist!";
        return 1;
    }
    if (!fs::exists(chip_config_path)) {
        logger(logERROR) << "Provided chip config file (=\""
                         << chip_config_filename << "\") does not exist!";
        return 1;
    }

    if (anamon_config_filename.empty()) {
        anamon_config_filename = rd53b::utils::default_anamon_configfile();
        logger(logINFO) << "No RD53b ana-mon config provided, will attempt to "
                           "use default file: "
                        << anamon_config_filename;
    }

    logger(logDEBUG)
        << "======================================================";
    logger(logDEBUG) << "program         : " << program_name;
    logger(logDEBUG) << "ana-mon config  : " << anamon_config_filename;
    logger(logDEBUG) << "hw config       : " << hw_config_filename;
    logger(logDEBUG) << "chip config     : " << chip_config_filename;
    logger(logDEBUG) << "n samples       : " << n_samples;
    logger(logDEBUG)
        << "======================================================";

    std::unique_ptr<RD53BAnalogMonitor> anamon =
        std::make_unique<RD53BAnalogMonitor>();
    if (!anamon->init(anamon_config_filename)) {
        logger(logERROR)
            << "Failed to initialize SCC analog monitor card control";
        return 1;
    }
    logger(logINFO) << "AnalogMonitorCard initialized!";

    //
    // setup the YARR items (the FrontEnd object <fe> and the HWController
    // object <hw>)
    //
    namespace rh = rd53b::helpers;
    auto hw = rh::spec_init(hw_config_filename);
    auto fe = rh::rd53b_init(hw, chip_config_filename);
    auto cfg = dynamic_cast<FrontEndCfg*>(fe.get());

    // clear tot memories
    rh::clear_tot_memories(hw, fe);
    std::this_thread::sleep_for(std::chrono::milliseconds(200));

    hw->setCmdEnable(cfg->getTxChannel());
    hw->setTrigEnable(0x0);

    // set monitor enable
    fe->writeRegister(&Rd53b::MonitorEnable, 1);
    std::this_thread::sleep_for(std::chrono::milliseconds(5));

    nlohmann::json histo_config;
    histo_config["histo_config"] = {
        {"bin_width", 0.001}, {"x_lo", 0}, {"x_hi", 10}};

    // create a vector of the VMUX_NAMES to sample
    std::vector<std::string> vmux_names;
    for (unsigned i = 0; i < 100; i++) {
        auto name = vmux_to_string(static_cast<VMUX_SEL>(i));
        if (!name.empty()) vmux_names.push_back(name);
    }

    // loop over the VMUX outputs and sample them
    size_t n_total = vmux_names.size();
    for (size_t i = 0; i < vmux_names.size(); i++) {
        auto current_vmux_out = vmux_names.at(i);
        logger(logDEBUG) << "Sampling VMUX output [" << std::setw(2) << (i + 1)
                         << "/" << std::setw(2) << n_total
                         << "]: " << current_vmux_out;

        // configure the RD53B VMUX and wait for some settling time
        fe->writeRegister(
            &Rd53b::MonitorV,
            static_cast<unsigned int>(vmux_from_string(current_vmux_out)));
        std::this_thread::sleep_for(std::chrono::milliseconds(5));

        auto h = rd53b::histogram1d::histo_init(histo_config);
        for (size_t n = 0; n < n_samples; n++) {
            std::this_thread::sleep_for(std::chrono::milliseconds(100));
            double val = anamon->read("VMUX_OUT");
            // fill the histogram
            h(val);
        }  // n

        // compute mean and stddev
        double mean = rd53b::histogram1d::mean(h);

        logger(logINFO) << "VMUX output [" << std::setw(2) << (i + 1) << "/"
                        << std::setw(2) << n_total << "] " << std::setw(20)
                        << vmux_names.at(i) << " = " << mean << " Volts";
        // double err = rd53b::histogram1d::stddev(h);
    }  // i

    return 0;
}
