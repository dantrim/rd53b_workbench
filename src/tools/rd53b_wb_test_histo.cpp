// std/stdl
#include <algorithm>
#include <experimental/filesystem>
#include <iomanip>
#include <iostream>
#include <random>
#include <sstream>
namespace fs = std::experimental::filesystem;

// histogramming
#include "Histogram1D.h"
#include "wb_file_utils.h"
#include "wb_string_utils.h"

// json
#include <nlohmann/json.hpp>
using json = nlohmann::json;

// labremote
#include "Logger.h"

int main(int argc, char* argv[]) {
    json h_config;
    h_config["histo_config"] = {{"bin_width", 0.05}, {"x_lo", -1}, {"x_hi", 1}};
    logger(logINFO) << "histo config: " << h_config.dump();

    auto h = rd53b::histogram1d::histo_init(h_config);

    int n = 100000;
    std::default_random_engine gen;
    std::normal_distribution<double> distribution(0.0, 0.2);
    for (int i = 0; i < n; i++) {
        double number = distribution(gen);
        h(number);
    }

    using namespace boost::histogram;
    std::ostringstream o;
    o << h;
    std::cout << o.str() << std::endl;
    int n_entries = rd53b::histogram1d::n_entries(h);
    std::cout << "n_entries    = " << n_entries << std::endl;

    std::cout << "histo mean   = " << rd53b::histogram1d::mean(h) << std::endl;
    std::cout << "histo stddev = " << rd53b::histogram1d::stddev(h)
              << std::endl;

    auto ind = indexed(h);
    auto max_val = rd53b::histogram1d::maximum(h);
    std::cout << "max element           = " << max_val << std::endl;
    // scale histo
    h /= 2;
    ind = indexed(h);
    max_val = rd53b::histogram1d::maximum(h);
    std::cout << "max element (scaled)  = " << max_val << std::endl;

    // add 2
    h += h;
    ind = indexed(h);
    max_val = rd53b::histogram1d::maximum(h);
    std::cout << "max element (add h)   = " << max_val << std::endl;

    // normalize
    h /= n_entries;
    ind = indexed(h);
    max_val = rd53b::histogram1d::maximum(h);
    std::cout << "max element (norm )   = " << max_val << std::endl;

    // save file
    json plot_config = {{"name", "hist_test"},
                        {"title", "test histogram"},
                        {"labels", {"x-label", "y-label"}},
                        {"color", "#147ef7"},
                        {"alpha", 0.5},
                        {"grid", true},
                        {"description", "RD53b"},
                        {"fit", "gaus"}};
    plot_config["plot_config"] = plot_config;

    // change axis range
    h = algorithm::reduce(
        h,
        algorithm::shrink(
            -0.3 + h_config["histo_config"]["bin_width"].get<double>(), 0.3));

    std::string program_name = rd53b::utils::remove_substr(
        fs::path(std::string(argv[0])).filename(), "rd53b_wb_");
    std::string output_dir = rd53b::utils::default_data_dir(program_name);

    rd53b::histogram1d::plot(h, plot_config, output_dir);

    return 0;
}
