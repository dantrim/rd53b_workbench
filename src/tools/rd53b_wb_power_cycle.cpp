// scc analog monitor card
#include "RD53BAnalogMonitor.h"

// workbench
#include "EquipmentHelpers.h"
#include "Histogram1D.h"
#include "RD53BHelpers.h"
#include "wb_file_utils.h"
#include "wb_string_utils.h"

// labremote
#include "Logger.h"
#include "PowerSupplyChannel.h"

// std/stl
#include <getopt.h>
#include <math.h>

#include <algorithm>
#include <experimental/filesystem>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <memory>
#include <sstream>
#include <string>
#include <vector>
namespace fs = std::experimental::filesystem;

// json // handled by YAR
#include <nlohmann/json.hpp>
// using json = nlohmann::json;

// yarr
#include "Rd53b.h"
#include "ScanHelper.h"
#include "SpecController.h"

struct option longopts_t[] = {{"hw", required_argument, NULL, 'c'},
                              {"chip", required_argument, NULL, 'r'},
                              {"debug", no_argument, NULL, 'd'},
                              {"equip", required_argument, NULL, 'e'},
                              {"temp", required_argument, NULL, 't'},
                              {"id", required_argument, NULL, 'q'},
                              {"n-samples", required_argument, NULL, 'n'},
                              {"help", no_argument, NULL, 'h'},
                              {0, 0, 0, 0}};

void print_help() {
    std::cout << "=========================================================="
              << std::endl;
    std::cout << " RD53B power-cycle" << std::endl;
    std::cout << std::endl;
    std::cout << " Usage: [CMD] [OPTIONS]" << std::endl;
    std::cout << std::endl;
    std::cout << " Options:" << std::endl;
    std::cout << "   --hw            JSON configuration file for hw controller"
              << std::endl;
    std::cout << "   --chip          JSON connectivity configuration for RD53B"
              << std::endl;
    std::cout << "   --equip         JSON configuration for lab equipment (PS)"
              << std::endl;
    std::cout << "   -t|--temp       operating temperature (deg. C)"
              << std::endl;
    std::cout << "   --id            chip ID" << std::endl;
    std::cout << "   -n|--n-samples  number of samples to record" << std::endl;
    std::cout << "   -d|--debug      turn on debug-level" << std::endl;
    std::cout << "   -h|--help       print this help message" << std::endl;
    std::cout << "=========================================================="
              << std::endl;
}

int main(int argc, char* argv[]) {
    std::string program_name = rd53b::utils::remove_substr(
        fs::path(std::string(argv[0])).filename(), "rd53b_wb_");
    std::string chip_config_filename = "";
    std::string hw_config_filename = "";
    std::string ps_config_filename = "";
    std::string operating_temp_str = "";
    unsigned int n_samples = 100;
    unsigned int chip_id = 0;
    bool debug = false;

    int c;
    while ((c = getopt_long(argc, argv, "c:dr:he:t:q:n:", longopts_t, NULL)) !=
           -1) {
        switch (c) {
            case 'c':
                hw_config_filename = optarg;
                break;
            case 'r':
                chip_config_filename = optarg;
                break;
            case 'd':
                logIt::incrDebug();
                debug = true;
                break;
            case 'e':
                ps_config_filename = optarg;
                break;
            case 't':
                operating_temp_str = optarg;
                break;
            case 'q':
                chip_id = std::stoi(optarg);
                break;
            case 'n':
                n_samples = std::stoi(optarg);
                break;
            case 'h':
                print_help();
                return 0;
                break;
            case '?':
            default:
                logger(logERROR) << "Invalid command-line argument provided";
                return 1;
        }  // switch
    }      // while

    // check the inputs
    fs::path hw_config_path(hw_config_filename);
    fs::path chip_config_path(chip_config_filename);
    fs::path ps_config_path(ps_config_filename);
    if (!fs::exists(hw_config_path)) {
        logger(logERROR) << "Provided HW config file (=\"" << hw_config_filename
                         << "\") does not exist!";
        return 1;
    }
    if (!fs::exists(chip_config_path)) {
        logger(logERROR) << "Provided chip config file (=\""
                         << chip_config_filename << "\") does not exist!";
        return 1;
    }
    if (!fs::exists(ps_config_path)) {
        logger(logERROR) << "Provided equipment config file (=\""
                         << ps_config_filename << "\") does not exist!";
        return 1;
    }

    operating_temp_str =
        rd53b::utils::replace_substr(operating_temp_str, " ", "");
    std::string operating_temp_title =
        rd53b::utils::replace_substr(operating_temp_str, "-", "neg");
    operating_temp_title =
        rd53b::utils::replace_substr(operating_temp_str, "+", "pos");
    float operating_temp = -300;
    if (!operating_temp_str.empty()) {
        try {
            operating_temp = std::stof(operating_temp_str);
            if (operating_temp < -273.15) {
                std::stringstream err;
                err << "Unphysical operating temperature provided (="
                    << operating_temp << " deg.-C)";
                logger(logERROR) << err.str();
                return 1;
            }
        } catch (std::exception& e) {
            std::stringstream err;
            err << "Invalid operating temperature provided (="
                << operating_temp_str << ")";
            logger(logERROR) << err.str() << ", exception: " << e.what();
            return 1;
        }
    }

    logger(logDEBUG)
        << "======================================================";
    logger(logDEBUG) << "program         : " << program_name;
    logger(logDEBUG) << "hw config       : " << hw_config_filename;
    logger(logDEBUG) << "chip config     : " << chip_config_filename;
    logger(logDEBUG) << "equp config     : " << ps_config_filename;
    logger(logDEBUG) << "operating temp. : "
                     << ((operating_temp_str == "") ? "room"
                                                    : operating_temp_str);
    logger(logDEBUG) << "chip id         : " << chip_id;
    logger(logDEBUG) << "n-samples       : " << n_samples;
    logger(logDEBUG)
        << "======================================================";

    //
    // setup the output data path
    //
    // fs::path p_output_data(rd53b::utils::default_data_dir(program_name));
    std::stringstream data_suffix;
    data_suffix << "chip0x" << std::hex << chip_id << std::dec;
    if (operating_temp_str != "") {
        data_suffix << "_T" << operating_temp_title;
    }
    std::string output_data_path =
        rd53b::utils::default_data_dir(program_name, data_suffix.str());
    logger(logDEBUG) << "output data path: " << output_data_path;

    std::unique_ptr<RD53BAnalogMonitor> anamon =
        std::make_unique<RD53BAnalogMonitor>();
    if (!anamon->init(rd53b::utils::default_anamon_configfile())) {
        logger(logERROR)
            << "Failed to initialize SCC analog monitor card control";
        return 1;
    }
    logger(logINFO) << "AnalogMonitorCard initialized!";

    //
    // setup the power supply
    //
    namespace eh = equipment::helpers;
    // std::string ps_name = "Agilent-goosevulture";
    std::string ps_name = "Rigol-inkfish";
    auto ps = eh::ps_init(ps_config_filename, ps_name);
    if (ps == nullptr) {
        logger(logERROR) << "Failed to load power supply configuration";
        return 1;
    }

    auto ps_vdda = eh::ps_channel_init(ps, ps_name, 1);
    auto ps_vddd = eh::ps_channel_init(ps, ps_name, 2);
    std::vector<std::shared_ptr<PowerSupplyChannel>> ps_channels = {ps_vdda,
                                                                    ps_vddd};
    // eh::ps_turn_on(ps_channels, 1.5, 1.8);

    //
    // setup the frontend
    //
    namespace rh = rd53b::helpers;
    auto hw = rh::spec_init(hw_config_filename);
    auto fe = rh::rd53b_init(hw, chip_config_filename);
    auto cfg = dynamic_cast<FrontEndCfg*>(fe.get());

    //
    // start power-cycle loop
    //
    unsigned measurement_count = 0;
    unsigned measurement_max = n_samples;

    nlohmann::json histo_config;
    histo_config["histo_config"] = {
        {"bin_width", 0.001}, {"x_lo", 0.0}, {"x_hi", 1.0}};
    auto h_start_up = rd53b::histogram1d::histo_init(histo_config);
    auto h_configured = rd53b::histogram1d::histo_init(histo_config);
    // auto h_reset = rd53b::histogram1d::histo_init(histo_config);

    float mean = 0.0;

    //
    // store output measurements
    //
    std::stringstream output_filename;
    output_filename << "data_power_cycle";
    if (operating_temp_str != "") {
        output_filename << "_T" << operating_temp_title;
    }
    output_filename << ".txt";
    fs::path p_output_file(output_data_path);
    p_output_file /= fs::path(output_filename.str());
    std::fstream datafile(std::string(p_output_file),
                          std::fstream::out | std::fstream::trunc);
    datafile << "#" << std::setw(12) << "IA_STARTUP," << std::setw(12)
             << "IA_CONFIG\n";  //," << std::setw(12) << "IA_RESET\n";

    ////foo
    logger(logINFO) << "Starting power-cycle loop...";
    size_t n_meas = 1;
    while (measurement_count < measurement_max) {
        eh::ps_turn_on(ps_channels, 1.5, 1.8);
        // std::this_thread::sleep_for(std::chrono::milliseconds(100));

        hw->setCmdEnable(cfg->getTxChannel());
        hw->setTrigEnable(0x0);

        double analog_current = 0.0;
        for (size_t i = 0; i < n_meas; i++)
            analog_current += ps_channels.at(0)->measureCurrent();
        analog_current /= static_cast<float>(n_meas);
        // double analog_current = ps_channels.at(0)->measureCurrent();
        h_start_up(analog_current);

        fe->configure();
        while (!hw->isCmdEmpty()) {
        }
        hw->flushBuffer();
        hw->setCmdEnable(cfg->getTxChannel());
        hw->setRxEnable(cfg->getRxChannel());
        std::this_thread::sleep_for(std::chrono::milliseconds(2000));

        double analog_current_configured = 0.0;
        for (size_t i = 0; i < n_meas; i++)
            analog_current_configured += ps_channels.at(0)->measureCurrent();
        analog_current_configured /= static_cast<float>(n_meas);
        // double analog_current_configured =
        // ps_channels.at(0)->measureCurrent();
        h_configured(analog_current_configured);

        // rh::rd53b_reset(hw, fe);
        // std::this_thread::sleep_for(std::chrono::milliseconds(1000));
        // double analog_current_reset = ps_channels.at(0)->measureCurrent();
        // h_reset(analog_current_reset);

        //
        // turn off the power
        //
        eh::ps_turn_off(ps_channels);
        // std::this_thread::sleep_for(std::chrono::milliseconds(100));

        if (measurement_count % 10 == 0 || debug) {
            logger(logINFO)
                << "  [" << measurement_count << "/" << measurement_max << "]"
                << " START-UP: " << analog_current
                << " A, CONFIGURED: " << analog_current_configured << " A";
            //<< " RESET: " << analog_current_reset << " A";
        }

        datafile << " " << std::fixed << std::setprecision(4) << std::setw(11)
                 << analog_current;
        datafile << ",";
        datafile << std::fixed << std::setprecision(4) << std::setw(11)
                 << analog_current_configured;
        //        datafile << ",";
        //        datafile << std::fixed << std::setprecision(4) <<
        //        std::setw(11) << analog_current_reset;
        datafile << "\n";

        measurement_count++;
    }  // while

    float mean_start_up = rd53b::histogram1d::mean(h_start_up);
    float stddev_start_up = rd53b::histogram1d::stddev(h_start_up);

    float mean_configured = rd53b::histogram1d::mean(h_configured);
    float stddev_configured = rd53b::histogram1d::stddev(h_configured);

    // float mean_reset = rd53b::histogram1d::mean(h_reset);
    // float stddev_reset = rd53b::histogram1d::stddev(h_reset);

    logger(logINFO) << "<I_A>:";
    logger(logINFO) << "    START-UP   = " << std::fixed << std::setprecision(4)
                    << " " << mean_start_up << " +/- " << stddev_start_up
                    << " A";
    logger(logINFO) << "    CONFIGURED = " << std::fixed << std::setprecision(4)
                    << " " << mean_configured << " +/- " << stddev_configured
                    << " A";
    // logger(logINFO) << "    RESET      = " << std::fixed <<
    // std::setprecision(4)
    //                << " " << mean_reset << " +/- " << stddev_reset << " A";

    // plot
    std::stringstream desc;
    desc << "RD53b (0x" << std::hex << chip_id << std::dec << ") ";
    desc << "power-cycling";
    if (operating_temp_str != "")
        desc << ", T = " << operating_temp << " deg. C";
    nlohmann::json plot_config = {{"labels", {"Analog Current [A]", "[a.u.]"}},
                                  {"color", "#2866F6"},
                                  {"alpha", 0.5},
                                  {"grid", true},
                                  {"description", desc.str()},
                                  {"fit", ""}};
    plot_config["plot_config"] = plot_config;

    std::stringstream common_name;
    common_name << "hist_power_cycle";
    if (operating_temp_str != "") {
        common_name << "_T" << operating_temp_title;
    }
    plot_config["plot_config"]["name"] = common_name.str() + "_STARTUP";
    plot_config["plot_config"]["title"] = "Start-up Analog Current";
    h_start_up /= rd53b::histogram1d::n_entries(h_start_up);  // normalize
    rd53b::histogram1d::plot(h_start_up, plot_config, output_data_path);

    plot_config["plot_config"]["name"] = common_name.str() + "_CONFIGURE";
    plot_config["plot_config"]["title"] = "Configured Analog Current";
    h_configured /= rd53b::histogram1d::n_entries(h_configured);  // normalize
    rd53b::histogram1d::plot(h_configured, plot_config, output_data_path);

    // plot_config["plot_config"]["name"] = common_name.str() + "_RESET";
    // plot_config["plot_config"]["title"] = "Post-reset Analog Current";
    // h_reset /= rd53b::histogram1d::n_entries(h_reset); // normalize
    // rd53b::histogram1d::plot(h_reset, plot_config, output_data_path);

    //
    // turn off the power
    //
    eh::ps_turn_off(ps_channels);

    return 0;
}
