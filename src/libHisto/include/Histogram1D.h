#ifndef HISTOGRAM1D_H
#define HISTOGRAM1D_H

// histogramming
#include <boost/histogram.hpp>
#include <boost/histogram/ostream.hpp>

// json
#include <nlohmann/json.hpp>

// std/stl
#include <stdlib.h>  // getenv

#include <exception>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>

// labremote
#include "Logger.h"

namespace rd53b {
namespace histogram1d {

// template <class Axes, class Storage>
// auto histo_vec_init(nlohmann::json& histo_config, size_t n) {
//    std::vector<boost::histogram::histogra<Axes, Storage>> vec;
//    for(size_t i = 0; i < n; i++)
//    {
//        auto h = histo_init(histo_conifg);
//        vec.push_back(h);
//    }
//    return vec;
//}

auto histo_init(nlohmann::json& histo_config) {
    auto cfg = histo_config["histo_config"];
    if (cfg.find("n_bins") != cfg.end()) {
        int n_bins = cfg.at("n_bins");
        float x_lo = cfg.at("x_lo");
        float x_hi = cfg.at("x_hi");
        return boost::histogram::make_histogram(
            boost::histogram::axis::regular<>(n_bins, x_lo, x_hi));
    } else if (cfg.find("bin_width") != cfg.end()) {
        float bin_width = cfg.at("bin_width");
        float x_lo = cfg.at("x_lo");
        float x_hi = cfg.at("x_hi");
        return boost::histogram::make_histogram(
            boost::histogram::axis::regular<>(
                boost::histogram::axis::step(bin_width), x_lo, x_hi));
    } else {
        std::stringstream e;
        e << "Invalid histogram config provided: " << cfg.dump();
        throw std::runtime_error(e.str());
    }
}

template <class Axes, class Storage>
double mean(const boost::histogram::histogram<Axes, Storage>& h) {
    auto entries = boost::histogram::algorithm::sum(h);
    bool empty = (entries == 0);
    if (empty) {
        logger(logINFO) << "HISTO EMPTY (entries = " << entries << ")";
        return std::nan("");
    }

    double weighted_sum = 0.0;
    double count = 0.0;
    float x_lo = 0.0;
    for (auto&& x : indexed(h)) {
        empty = false;
        if (x.index() == 0) x_lo = x.bin().lower();
        double data = static_cast<double>(*x);
        double bin_width = (x.bin().upper() - x.bin().lower());
        weighted_sum +=
            data * (((x.index()) * bin_width) + x_lo + (bin_width * 0.5));
        count += data;
    }  // x
    return weighted_sum / count;
}

template <class Axes, class Storage>
double stddev(const boost::histogram::histogram<Axes, Storage>& h) {
    double entries = boost::histogram::algorithm::sum(h);
    bool empty = (entries == 0);
    if (empty) {
        logger(logINFO) << "HISTO EMPTY (entries = " << entries << ")";
        return std::nan("");
    }
    double mean_val = mean(h);
    double mu = 0;
    float x_lo = 0.0;
    for (auto&& x : indexed(h)) {
        if (x.index() == 0) x_lo = x.bin().lower();
        double data = static_cast<double>(*x);
        double bin_width = (x.bin().upper() - x.bin().lower());
        mu +=
            data * pow((((x.index()) * bin_width) + x_lo + (bin_width / 2.0)) -
                           mean_val,
                       2);
    }
    return sqrt(mu / entries);
}

template <class Axes, class Storage>
unsigned n_entries(const boost::histogram::histogram<Axes, Storage>& h) {
    auto entries = boost::histogram::algorithm::sum(h);
    return entries;
}

template <class Axes, class Storage>
double maximum(const boost::histogram::histogram<Axes, Storage>& h) {
    auto ind = boost::histogram::indexed(h);
    auto max = std::max_element(ind.begin(), ind.end());
    return *max;
}

template <class Axes, class Storage>
double minimum(const boost::histogram::histogram<Axes, Storage>& h) {
    auto ind = boost::histogram::indexed(h);
    auto min = std::min_element(ind.begin(), ind.end());
    return *min;
}

template <class Axes, class Storage>
void to_file(const boost::histogram::histogram<Axes, Storage>& h,
             std::string tmp_filename, float& x_lo, float& x_hi) {
    std::fstream file(tmp_filename, std::fstream::out | std::fstream::trunc);
    int first_data_index = -1;
    int last_data_index = -1;
    int min_bin = -1;
    int max_bin = -1;
    for (auto&& x : indexed(h)) {
        if (min_bin < 0) min_bin = x.index();
        max_bin = x.index();
        if (((*x) > 0)) {
            if (first_data_index < 0) {
                first_data_index = x.index();
            }
            last_data_index = x.index();
        }
    }

    size_t n_max = 10;
    size_t n = 0;
    while (n < n_max) {
        int tmp = (first_data_index - 1);
        if (tmp < min_bin) break;
        first_data_index = tmp;
        n++;
    }
    n = 0;
    while (n < n_max) {
        int tmp = (last_data_index + 1);
        if (tmp > max_bin) break;
        last_data_index = tmp;
        n++;
    }

    bool first_val_set = false;
    bool last_val_set = false;
    for (auto&& x : indexed(h)) {
        if (x.index() < first_data_index) continue;
        if (x.index() > last_data_index) break;
        if (x.index() == first_data_index) {
            x_lo = x.bin().lower();
        }
        if (x.index() == last_data_index) {
            x_hi = x.bin().upper();
        }
        file << *x << " ";
    }
    file << std::endl;
    file.close();
}

template <class Axes, class Storage>
void plot(boost::histogram::histogram<Axes, Storage>& h,
          nlohmann::json& plot_config, std::string output_path) {
    auto cfg = plot_config.at("plot_config");
    std::string plot_name = cfg.at("name");
    std::string title = cfg.at("title");
    auto labels = cfg.at("labels").get<std::vector<std::string>>();
    std::string fill_color = cfg.at("color");
    bool do_grid = cfg.at("grid");
    std::string description = "";
    if (cfg.find("description") != cfg.end())
        description = cfg.at("description");
    std::string fit = "";
    if (cfg.find("fit") != cfg.end()) fit = cfg.at("fit");

    float x_lo = 0.0;
    float x_hi = 0.0;
    float bin_width = 0.0;
    int n_bins = 0;
    for (auto&& x : indexed(h)) {
        if (x.index() == 0) x_lo = x.bin().lower();
        x_hi = x.bin().upper();
        bin_width = (x.bin().upper() - x.bin().lower());
        n_bins++;
    }

    double mean = rd53b::histogram1d::mean(h);
    double sigma = rd53b::histogram1d::stddev(h);
    double five_sigma = 5.0 * sigma;
    // if(five_sigma < 0.050)
    //    five_sigma = 0.050;
    // x_lo = mean - five_sigma;
    // x_hi = mean + five_sigma;
    // logger(logINFO) << "Rebinning to +/- 5 sigma vals: " << (x_lo +
    // bin_width) << ", " << x_hi; h = boost::histogram::algorithm::reduce(h,
    // boost::histogram::algorithm::shrink(x_lo + bin_width, x_hi));

    std::string tmp_filename = "/tmp/" + std::string(getenv("USER")) +
                               "/tmp_rd53b_wb_histo_" + plot_name + ".dat";
    rd53b::histogram1d::to_file(h, tmp_filename, x_lo, x_hi);

    std::stringstream output_filename;
    output_filename << output_path << "/" << plot_name << ".pdf";
    std::string cmd = "gnuplot | epstopdf -f > " + output_filename.str();
    FILE* gnu = popen(cmd.c_str(), "w");

    auto maxy = rd53b::histogram1d::maximum(h);
    maxy *= 1.15;

    fprintf(gnu,
            "set terminal postscript enhanced color \"Helvetica\" 18 eps\n");
    fprintf(gnu, "unset key\n");
    fprintf(gnu, "set title \"%s\"\n", title.c_str());
    fprintf(gnu, "set xlabel \"%s\"\n", labels.at(0).c_str());
    fprintf(gnu, "set ylabel \"%s\"\n", labels.at(1).c_str());
    fprintf(gnu, "set xrange[%f:%f]\n", x_lo, x_hi);
    fprintf(gnu, "set yrange[0:%f]\n", maxy);
    if (do_grid) fprintf(gnu, "set grid\n");
    fprintf(gnu, "set style line 1 lt 3\n");
    std::stringstream fill_alpha;
    fill_alpha << "set style fill solid";
    if (cfg.find("alpha") != cfg.end()) fill_alpha << " " << cfg.at("alpha");
    fill_alpha << "\n";
    fprintf(gnu, fill_alpha.str().c_str());

    fprintf(gnu, "set boxwidth %f*1.0 absolute\n", bin_width);
    fprintf(gnu, "bin_width = %f\n", bin_width);

    fprintf(gnu, "set boxwidth %f*1.0 absolute\n", bin_width);
    std::stringstream data_file;
    data_file << tmp_filename;
    // data_file << plot_name << ".dat";

    // labels
    std::stringstream label;

    // top of plot description
    if (!description.empty()) {
        label << "set label 1 at graph 0.01, 1.02 font \"Helvetica,12\"\n";
        fprintf(gnu, label.str().c_str());
        label.str("");
        label << "set label 1 \"" << description
              << "\" textcolor rgb \"black\"\n";
        fprintf(gnu, label.str().c_str());
        label.str("");
    }

    if (!fit.empty() && fit == "gaus") {
        fprintf(gnu, "set table \"foo.temp\"\n");
        fprintf(gnu,
                "plot \"%s\" matrix u ((($1)*(%f))+%f+(%f/2)):3 with boxes "
                "fill lc rgb \"%s\"\n",
                data_file.str().c_str(), bin_width, x_lo, bin_width,
                fill_color.c_str());
        fprintf(gnu, "unset table\n");

        fprintf(gnu, "set fit quiet\n");
        fprintf(gnu, "a=1.0;mean=%f;sigma=%f\n", mean, sigma);
        fprintf(
            gnu,
            "gauss(x)=a/(sqrt(2*pi)*sigma)*exp(-(x-mean)**2/(2*sigma**2))\n");
        fprintf(gnu, "fit gauss(x) \"foo.temp\" via a,sigma,mean\n");
        fprintf(gnu, "chi2 = (FIT_STDFIT*FIT_STDFIT)\n");

        // fit params -- mu
        label << "set label 2 at graph 0.03, 0.95 font \"Helvetica,12\"\n";
        fprintf(gnu, label.str().c_str());
        label.str("");
        label << "set label 2 sprintf(\"{/Symbol m} = %%3.3f\",mean)\n";
        fprintf(gnu, label.str().c_str());
        label.str("");

        // fit params -- sigma
        label << "set label 3 at graph 0.03, 0.91 font \"Helvetica,12\"\n";
        fprintf(gnu, label.str().c_str());
        label.str("");
        label << "set label 3 sprintf(\"{/Symbol s} = %%3.3f\", sigma)\n";
        fprintf(gnu, label.str().c_str());
        label.str("");

        // fit params -- chi2
        label << "set label 4 at graph 0.03, 0.86 font \"Helvetica,12\"\n";
        fprintf(gnu, label.str().c_str());
        label.str("");
        label << "set label 4 sprintf(\"{/Symbol c}^2 = %%.3e\", chi2)\n";
        fprintf(gnu, label.str().c_str());
        label.str("");

        fprintf(gnu,
                "plot \"%s\" matrix u ((($1)*(%f))+%f+(%f/2)):3 with boxes "
                "fill lc rgb \"%s\", gauss(x) lw 3 lc rgb \"%s\"\n",
                data_file.str().c_str(), bin_width, x_lo, bin_width,
                fill_color.c_str(), fill_color.c_str());
    } else {
        // fit params -- mu
        label << "set label 2 at graph 0.03, 0.95 font \"Helvetica,12\"\n";
        fprintf(gnu, label.str().c_str());
        label.str("");
        char val[50];
        sprintf(val, "%3.3f", mean);
        label << "set label 2 \"Mean = " << val << "\"\n";
        fprintf(gnu, label.str().c_str());
        label.str("");

        // fit params -- sigma
        label << "set label 3 at graph 0.03, 0.91 font \"Helvetica,12\"\n";
        fprintf(gnu, label.str().c_str());
        label.str("");
        sprintf(val, "%3.3f", sigma);
        label << "set label 3 \"Std. Dev. = " << val << "\"\n";
        fprintf(gnu, label.str().c_str());
        label.str("");

        fprintf(gnu,
                "plot \"%s\" matrix u ((($1)*(%f))+%f+(%f/2)):3 with boxes "
                "fill lc rgb \"%s\"\n",
                data_file.str().c_str(), bin_width, x_lo, bin_width,
                fill_color.c_str());
    }

    pclose(gnu);
    logger(logINFO) << "Saving figure: " << output_filename.str();
}

};  // namespace histogram1d
};  // namespace rd53b

#endif  // HISTOGRAM1D_H
