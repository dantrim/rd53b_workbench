#!/bin/env python

import sys
import matplotlib.pyplot as plt
import numpy as np

from argparse import ArgumentParser

def main() :

    parser = ArgumentParser()
    parser.add_argument("-i", "--input", required = True)
    parser.add_argument("-p", "--plot", action = "store_true", default = False)
    parser.add_argument("-c", "--chip", required = True)
    parser.add_argument("-s", "--scc", required = True)
    args = parser.parse_args()

    input_filename = args.input
    do_plots = args.plot
    chip_number = args.chip
    scc_number = args.scc

    trim_bits_vals = []
    iref_vals = []
    iref_err_vals = []
    with open(input_filename, "r") as infile :
        for line in infile :
            line = line.strip()
            if line.startswith("#") : continue
            trim_bits, iref, err = [float(x) for x in line.split()]
            trim_bits_vals.append(trim_bits)
            iref_vals.append(iref)
            iref_err_vals.append(err)

    if do_plots :
        fig, ax = plt.subplots(1,1)
        ax.tick_params(axis = "both", which = "both", labelsize = 14, direction = "in",
                labelleft = True, bottom = True, top = True, right = True, left = True)
        ax.grid(color = "k", which = "both", linestyle = "-", lw = 0.5, alpha = 0.2)
        ax.set_xlabel(r"I_Ref trim bits", horizontalalignment = "right", x = 1, fontsize = 14)
        ax.set_ylabel(r"I_Ref [µA]", horizontalalignment = "right", y = 1, fontsize = 14)
        ax.set_xlim([-1,16])
        ax.set_ylim([0.95 * iref_vals[0], 1.05 * iref_vals[-1]])
        ax.set_xticks(np.arange(0,16,1))

        ax.errorbar(trim_bits_vals, iref_vals, yerr = iref_err_vals, 
            ecolor = "k",
            elinewidth = 1,
            linestyle = None,
            linewidth = 0,
            marker = "o",
            markerfacecolor = "k",
            markeredgecolor = "k"
        )

        for i, iref_val in enumerate(iref_vals) :
            x_pos = trim_bits_vals[i] - 0.5
            y_pos = 0.98 * iref_val
            ax.text(x_pos, y_pos, f"{iref_val:0.2f}")

        ax.text(0.01, 1.02, f"Chip: {hex(chip_number)}, SCC: {scc_number}", transform = ax.transAxes, fontsize = 10, ha = "left")
        fig.show()
        x = input()
        outname = input_filename.replace(".txt", ".pdf")
        fig.savefig(outname, bbox_inches = "tight")


if __name__ == "__main__" :
    main()
