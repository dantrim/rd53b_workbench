#!/bin/env python

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.gridspec import GridSpec
import sys
from argparse import ArgumentParser
from math import ceil
from scipy.optimize import curve_fit


# NOTES:
# VRef_ADC chip 4-6: 0.8188 +/- 0.0307 V

class triple_pad_canvas() :

    def __init__(self, name = "", figsize = (7, 10)) :
        self._name = name
        self._figsize = figsize

        self._fig = None
        self._labels = []
        self._upper_pad = None
        self._middle_pad = None
        self._lower_pad = None

        self._x_bounds = []
        self._y_bounds = []

    @property
    def name(self) :
        return self._name

    @property
    def figsize(self) :
        return self._figsize
    @figsize.setter
    def figsize(self, val) :
        if len(val) != 2 :
            raise ValueError('%s : requested figsize (%s) is incorrect size (size=%d, expect=2)' \
                % ( type(self).__name__, val, len(val) ) )
        self._figsize = figsize

    @property
    def fig(self) :
        return self._fig

    @property
    def labels(self) :
        return self._labels
    @labels.setter
    def labels(self, val) :
        if len(val) != 2 :
            raise ValueError('%s : Attempting to set labels %s that are incorrect size (size=%d, expect=2)'\
                % ( type(self).__name__, val, len(val) ) )
        self._labels = val

    @property
    def upper_pad(self) :
        return self._upper_pad

    @property
    def middle_pad(self) :
        return self._middle_pad

    @property
    def lower_pad(self) :
        return self._lower_pad

    @property
    def x_bounds(self) :
        return self._x_bounds
    @x_bounds.setter
    def x_bounds(self, val) :
        if len(val) != 2 :
            raise ValueError('%s : Attempting to set x-axis bounds %s that are incorrect size (size=%d, expect=2)' \
                % ( type(self).__name__, val, len(val) ) )
        self._x_bounds = val

    @property
    def y_bounds(self) :
        return self._y_bounds
    @y_bounds.setter
    def y_bounds(self, val) :
        if len(val) != 2 :
            raise ValueError('%s : Attempting to set y-axis bounds %s that are incorrect size (size=%d, expect=2)' \
                % ( type(self).__name__, val, len(val) ) )
        self._y_bounds = val


    def build(self) :

        fig = plt.figure(figsize = self.figsize)
        fig.set_size_inches(self.figsize[0], self.figsize[1])
        #fig.set_size_inches(pad_width, pad_height)
        grid = GridSpec(100,100)
        upper = fig.add_subplot(grid[0:65, :])
        middle = fig.add_subplot(grid[66:83, :])
        lower = fig.add_subplot(grid[85:100, :])

        # axes
        upper.set_xticklabels([])
        middle.set_xticklabels([])
        upper.set_xlim(self.x_bounds[0], self.x_bounds[1])
        middle.set_xlim(self.x_bounds[0], self.x_bounds[1])
        lower.set_xlim(self.x_bounds[0], self.x_bounds[1])

        #middle.set_ylim(0.0, 2.0)
        #lower.set_ylim(0.0, 2.0)

        for ax in [upper, middle, lower] :
            #ax.tick_params(axis = 'both', which = 'both', labelsize = 14, direction = "in")
            ax.tick_params(axis = "both", which = "both", direction = "in", labelleft = True, bottom = True, top = True, right = True, left = True)
            which_grid = 'both'
            ax.grid( color = 'k', which=which_grid, linestyle = '--', lw = 1, alpha = 0.1)
        ax_x = upper.get_position().x0
        lower.set_xlabel(self.labels[0],
                #horizontalalignment = 'right',
                #x = 1.0,
                fontsize = 16)

        #upper.set_ylabel(self.labels[1],
        #        horizontalalignment = 'right',
        #        y = 1.0,
        #        fontsize = 18)

        #middle.set_ylabel("Num / Den", fontsize = 12)
        #lower.set_ylabel("Num / Den", fontsize = 12)

        #upper.get_yaxis().set_label_coords(-0.16, 1.0)
        #middle.get_yaxis().set_label_coords(-0.16, 0.5)
        #lower.get_yaxis().set_label_coords(-0.16, 0.5)

        self._fig = fig
        self._upper_pad = upper
        self._middle_pad = middle
        self._lower_pad = lower

def func_linear(x, m, b) :
    x = np.array(x)
    return x * m + b

def get_dnl(measured, ideal, lsb_value) :

    if len(measured) != len(ideal) :
        return []

    dnl_vals = [0]
    for i in range(len(measured)) :
        if i == 0 : continue
        dnl = measured[i] - measured[i-1] - lsb_value
        dnl_vals.append(dnl)
    return np.array(dnl_vals)

def get_inl(dnl_vals) :

    dnl_arr = np.array(dnl_vals)
    inl = np.cumsum(dnl_arr)
    return inl

def dac_info_from_name(dac_name) :

    # return DAC n-bits and voltage reference

    return {
        "DIFF_FE_VTH2" : [10, 0.6],
        "DIFF_FE_VTH1_MAIN" : [10, 0.6],
        "DIFF_FE_VTH1_LEFT" : [10, 0.6],
        "DIFF_FE_VTH1_RIGHT" : [10, 0.6],
        "VCAL_MED" : [12,-1],
        "VCAL_HI" : [12,-1]
    }[dac_name]

def dac_name_from_filename(input_filename) :

    dac = input_filename.strip().split("/")[-1]
    dac = dac.replace(".txt","")
    dac = dac.replace("data_dac_meas_","")
    return dac

def lsb_value(vref, n_bits) :
    return (float(vref) / (2**int(n_bits) - 1))

def ideal_dac_output(dac_settings, dac_info) :

    n_bits, vref = int(dac_info[0]), float(dac_info[1])
    lsb = lsb_value(vref, n_bits)
    ideal_output = []
    for dac in dac_settings :
        ideal_output.append( lsb * dac )
    return ideal_output

def roundup(x):
    return int(ceil(x / 10.0)) * 10

def roundup_to(x, v) :
    return int(ceil(x / float(v)))  * float(v)

def moving_average(a, n=3) :
    ret = np.cumsum(a, dtype=float)
    ret[n:] = ret[n:] - ret[:-n]
    return ret[n - 1:] / n

def main() :

    parser = ArgumentParser()
    parser.add_argument("-i", "--input", required = True, help = "Input file of DAC measurements")
    parser.add_argument("-c", "--chip", required = True, help = "Chip number")
    parser.add_argument("-s", "--scc", required = True, help = "SCC number")
    parser.add_argument("--vref-adc", default = -1, help = "Provide a VRef_ADC (GADC) value")
    parser.add_argument("--line-fit", default = False, action = "store_true",
            help = "Perform a linear fit of the measured DAC response"
    )
    args = parser.parse_args()

    do_linear_fit = args.line_fit
    dac_name = dac_name_from_filename(args.input)
    chip_number = int(args.chip, 16)
    scc_number = int(args.scc, 10)
    print(f"Chip. #: {hex(chip_number)}, SCC #: {scc_number}")
    print(f"Data loaded for DAC: {dac_name}")

    dac_info = dac_info_from_name(dac_name)
    vref_adc = float(args.vref_adc)
    if dac_info[1] < 0 :
        if vref_adc < 0 :
            print(f"ERROR You requested DAC {dac_name} but did not provide a VRef_ADC value")
            sys.exit()
        dac_info[1] = vref_adc

    dac_setting = []
    dac_output = []
    with open(args.input, "r") as infile :
        for line in infile :
            line = line.strip()
            if line.startswith("#") : continue
            dac_input_setting, analog_output, err = [x.strip() for x in line.split(",")]
            dac_input_setting = int(dac_input_setting)
            analog_output = float(analog_output)

            #analog_output -= 0.0047
            #analog_output *= 0.996
            #analog_output -= 0.0055
            #analog_output *= 0.999
            #if dac_input_setting > 50 : break
            err = float(err)
            dac_setting.append(dac_input_setting)
            dac_output.append(analog_output)

    ##
    ## get the ideal analog output for the given dac settings
    ##
    ideal_analog_output = ideal_dac_output(dac_setting, dac_info)

    ##
    ## setup the plot canvas
    ##
    pad_width = 15
    pad_height = 10
    canvas = triple_pad_canvas(name = "dac_test", figsize = (pad_width, pad_height))
    canvas.x_bounds = [0, int(2**dac_info[0]) + 1]
    x_label = f"DAC {dac_name} Setting [counts]"
    canvas.labels = [x_label, ""]
    canvas.build()
    
    canvas.upper_pad.set_ylabel(f"Analog output [V]", fontsize = 16)
    canvas.middle_pad.set_ylabel(f"DNL [LSB]", fontsize = 16)
    canvas.middle_pad.tick_params(axis="both", which="both", labelsize = 8)
    canvas.lower_pad.set_ylabel(f"INL [LSB]", fontsize = 16)
    canvas.lower_pad.tick_params(axis="y", which = "both", labelsize = 8)

    ##
    ## plot the DAC response
    ##
    #fig, ax = plt.subplots(1,1)
    #fig.set_size_inches(pad_width, pad_height)
    #ax.tick_params(axis = "both", which = "both", labelsize = 16, direction = "in",
    #            labelleft = True, bottom = True, top = True, right = True, left = True)
    #ax.grid(color = "k", which = "both", linestyle = "-", lw = 0.5, alpha = 0.1)
    #ax.set_xlabel(f"DAC {dac_name} Setting [counts]", fontsize = 16)
    #ax.set_ylabel(f"Analog output [V]", fontsize = 16)

    ##
    ## offset and gain errors
    ##
    lsb = lsb_value(vref = dac_info[1], n_bits = dac_info[0])
    print(f"lsb value = {lsb:0.5f} V")
    offset_correction = dac_output[0] - ideal_analog_output[0]
    print(f"offset_correction = {offset_correction}")
    print(f"vref = {dac_info[1]}")
    print(f"dac_output[-1] = {dac_output[-1]}")
    print(f"dac_info[1] = {dac_info[1]}")
    print(f"ideal_analog_output[-1] = {ideal_analog_output[-1]}")

    dac_max_offset_corrected = dac_output[-1] - offset_correction
    vref_minus_1lsb = float(dac_info[1]) - lsb
    gain_error_percent = dac_max_offset_corrected / vref_minus_1lsb
    gain_error_percent -= 1
    gain_error_percent *= 100.

    #gain_error_percent = 100.0 * ( ((dac_output[-1] - offset_correction) / (float(dac_info[1]) - 1 * lsb) ) - 1)
    gain_error_absolute = gain_error_percent / 100.0
    print(f"gain_error = {gain_error_percent:0.2f} %")
    gain_correction = 1.0 / (1.0 + gain_error_absolute)
    print(f"gain_correction = {gain_correction:0.3f}")


    ##
    ## plot the response of the ideal DAC with the given voltage reference
    ##
    canvas.upper_pad.plot(dac_setting, ideal_analog_output, marker = "o", linewidth = 0, markersize = 1, color = "k",
            label = f"Ideal"
    )
    #ax.plot(dac_setting, ideal_analog_output, marker = "o", linewidth = 0, markersize = 2, color = "k",
    #        label = f"Ideal"
    #)

    ##
    ## measured values
    ##
    label = f"{dac_name} meas. (Offset error: {offset_correction:0.3f} V, Gain error: {gain_error_percent:0.3f}%)"
    #ax.plot(dac_setting, dac_output, marker = "o", linewidth = 0, markersize = 2, color = "r",
    #        label = label
    #        #label = f"{dac_name} meas."
    #)
    canvas.upper_pad.plot(dac_setting, dac_output, marker = "o", linewidth = 0, markersize = 1, color = "r",
            label = label
            #label = f"{dac_name} meas."
    )

    ##
    ## do linear fit
    ##
    if do_linear_fit :
        #popt, pcov = curve_fit(func_powerlaw, test_X[1:], test_Y[1:], p0=np.asarray([0.5,0.5,0.5]), maxfev=2000)
        ideal_slope = (0.8188 - 0) / (4095 - 0)
        ideal_intercept = 0
        popt, pcov = curve_fit(func_linear, dac_setting, dac_output, p0 = [ideal_slope, ideal_intercept])
        fit_param_names = ["Slope", "y-Intercept"]
        params = zip(fit_param_names, popt)
        print(f"Linear fit results:")
        for p in params :
            name, val = p
            print(f"  {name}: {val:.4e}")
        label = f"Linear Fit (Slope: {popt[0]:.3e}, Intercept: {popt[1]:.3e})"
        #ax.plot(dac_setting, func_linear(dac_setting, *popt), "b--", label = label)
        canvas.upper_pad.plot(dac_setting, func_linear(dac_setting, *popt), "b--", label = label)

    #ax.plot( [dac_setting[-1], dac_setting[-1]], [0, dac_info[1]], "r--", alpha = 0.5, linewidth = 1)


    # legend
    #ax.legend(loc = "best", frameon = False)
    canvas.upper_pad.legend(loc = "best", frameon = False)

    # text
    #ax.text(0.01, 1.02, f"Chip: {hex(chip_number)}, SCC: {scc_number}", transform = ax.transAxes, fontsize = 10, ha = "left")
    canvas.upper_pad.text(0.01, 1.02, f"Chip: {hex(chip_number)}, SCC: {scc_number}", transform = canvas.upper_pad.transAxes, fontsize = 10, ha = "left")

    #fig.show()
    #canvas.fig.show()
    #x = input()
    #outname = f"dac_meas_{dac_name}_trunc.pdf"
    #fig.savefig(outname, bbox_inches = "tight")

    ##
    ## linearity errors
    ##

    dnl = get_dnl(dac_output, ideal_analog_output, lsb)
    inl = get_inl(dnl)
    dnl /= ideal_analog_output[1] # units of LSB
    inl /= ideal_analog_output[1] # units of LSB

    # plot dnl
    #fig, ax = plt.subplots(1,1)
    #pad_width = 15
    #pad_height = 4
    #fig.set_size_inches(pad_width, pad_height)
    #@canvas.middle_pad.tick_params(canvas.middle_padis = "both", which = "both", labelsize = 16, direction = "in",
    #@        labelleft = True, bottom = True, top = True, left = True, right = True)
    #@canvas.middle_pad.grid(color = "k", which = "both", linestyle = "-", lw = 0.5, alpha = 0.1)
    #@canvas.middle_pad.set_ylabel("DNL [LSB]", fontsize = 16)
    #@canvas.middle_pad.set_xlabel(f"DAC {dac_name} Setting [counts]", fontsize = 16)
    for idnl, dnl_val in enumerate(dnl) :
        canvas.middle_pad.plot([dac_setting[idnl], dac_setting[idnl]], [0, dnl_val], "k--", linewidth = 1.5)
        canvas.middle_pad.plot([dac_setting[idnl]], [dnl_val], "ko", markersize = 4)
        #canvas.middle_pad.plot([dac_setting[idnl], dac_setting[idnl]], [0, dnl_val], "k-", linewidth = 1)
        #canvas.middle_pad.plot([dac_setting[idnl]], [dnl_val], "ko", markersize = 0.6)
    #canvas.middle_pad.plot(dac_setting, dnl, "ko", markersize = 2, linewidth = 1)
    #canvas.middle_pad.set_xlim([dac_setting[0], dac_setting[-1]])

    # text
    #ax.text(0.01, 1.02, f"Chip: {hex(chip_number)}, SCC: {scc_number}", transform = ax.transAxes, fontsize = 10, ha = "left")

    # running average
    dnl_moving_average = moving_average(dnl, n = 10)
    #for i, inl_val in enumerate(inl) :
    #    numerator = inl_val
    #    denominator = i+1
    #    dnl_running_average.append( float(numerator) / float(denominator))
    canvas.middle_pad.plot(dac_setting[9:], dnl_moving_average, "g-", linewidth = 1.5, alpha = 0.8, label = "Local average")

    #ax.plot([dac_setting[0], dac_setting[-1]], [0,0], "r--")
    canvas.middle_pad.text(0.01, 0.9, f"1 LSB = {lsb*1000.0:.1f} mV", transform = canvas.middle_pad.transAxes, ha = "left")
    canvas.middle_pad.legend(loc = "lower right", frameon = False)

    maxy = roundup_to(max(dnl), 5)
    miny = min(dnl)
    if miny < 0 :
        abs_val = abs(miny)
        miny = roundup_to(abs_val, 5)
        miny = -1.0 * miny
    else :
        miny = roundup_to(dnl, 5)
    yticks = np.arange(miny, maxy+5, 2)
    canvas.middle_pad.set_yticks(yticks)
    canvas.middle_pad.set_ylim([miny,maxy])


    
    #fig.show()

    #x = input()
    #outname = f"dac_meas_{dac_name}_DNL_trunc.pdf"
    #fig.savefig(outname, bbox_inches = "tight")

    # plot inl
    #fig, ax = plt.subplots(1,1)
    #pad_width = 15
    #pad_height = 4
    #fig.set_size_inches(pad_width, pad_height)
    #ax.tick_params(axis = "both", which = "both", labelsize = 16, direction = "in",
    #        labelleft = True, bottom = True, top = True, left = True, right = True)
    #ax.grid(color = "k", which = "both", linestyle = "-", lw = 0.5, alpha = 0.1)
    #ax.set_ylabel("INL [LSB]", fontsize = 16)
    #ax.set_xlabel(f"DAC {dac_name} Setting [counts]", fontsize = 16)
    canvas.lower_pad.plot(dac_setting, inl, "k-", markersize = 2, linewidth = 1)
    canvas.lower_pad.plot([dac_setting[0], dac_setting[-1]], [0,0], "r--")
    #canvas.lower_pad.set_xlim([dac_setting[0], dac_setting[-1]])

    maxy = roundup(max(inl)) + 10
    miny = min(inl)
    if miny < 0 :
        abs_val = abs(miny)
        miny = roundup(abs_val)
        miny = -1.0 * miny
    else :
        miny = roundup(miny) 
    yticks = np.arange(miny, maxy+10, 10)
    canvas.lower_pad.set_yticks(yticks)
    canvas.lower_pad.set_ylim([miny, maxy])

    # text
    #ax.text(0.01, 1.02, f"Chip: {hex(chip_number)}, SCC: {scc_number}", transform = ax.transAxes, fontsize = 10, ha = "left")
    #ax.text(0.01, 0.9, f"1 LSB = {lsb*1000.0:.1f} mV", transform = ax.transAxes, ha = "left")
    
    #fig.show()
    #x = input()
    outname = f"dac_meas_{dac_name}.pdf"
    canvas.fig.show()
    x = input()
    canvas.fig.savefig(outname, bbox_inches = "tight")
    
        

if __name__ == "__main__" :
    main()
